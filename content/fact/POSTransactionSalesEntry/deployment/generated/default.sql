SELECT
  -- System Information
  TSE.stage_id AS StageID
  , TSE.Company_Id AS CompanyID
  , TSE.data_connection_id AS DataConnectionID
  -- Dimensions
  , TSE.[Shift No_] AS StoreCode
  , TSE.[POS Terminal No_] AS POSTerminalCode
  , TH.[Created on POS Terminal] AS CreatedonPOSTerminal
  , TSE.[Promotion No_] AS SpecialsCode
  , TSE.[Periodic Disc_ Group] AS PeriodicDiscountGroup
  , TSE.[Sales Staff] AS SalesStaffCode
  , TSE.[Staff ID] AS StaffCode
  , TSE.[Sales Type] AS SalesTypeCode
  , TSE.[Date] AS ReceiptDate
  , CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, TSE.TIME), 0)), 108) AS ReceiptTime
  , TSE.[Customer No_] AS CustomerCode
  , TSE.[Section] AS SectionCode
  , TSE.Shelf AS ShelfCode
  , TSE.[Item No_] AS ItemCode
  , TSE.[Variant Code] AS ItemVariantCode
  , CASE 
    WHEN TSE.[Variant Code] <> ''
      THEN 1
    ELSE 0
    END AS ItemVariantYesNoCode
  , TH.[Sale Is Return Sale] AS SalesIsReturnSaleYesNoCode
  , TSE.[Transaction Code] AS TransactionCode
  , TSE.[Line No_] AS TransactionLineNo
  , CASE 
    WHEN (TSE.[Total Discount] <> 0)
      AND (TSE.[Line Discount] = 0)
      THEN 7 -- 'POS Total Discount'
    WHEN (TSE.[Line Discount] <> 0)
      AND (TSE.[Total Discount] = 0)
      AND (TSE.[Customer Discount] = 0)
      AND (TSE.[Cust_ Invoice Discount] = 0)
      AND (TSE.[Infocode Discount] = 0)
      THEN 10 --'POS Line Discount'
    WHEN (TSE.[Customer Discount] <> 0)
      THEN 1 -- 'Customer Discount'
    WHEN TSE.[Periodic Disc_ Group] <> ''
      THEN 0 --'Periodic Discount'
    WHEN (TSE.[Receipt No_] <> '')
      AND (TSE.[Deal Line] = 1)
      THEN 6 --'Deal'
    WHEN TSE.[Promotion No_] <> ''
      THEN 5 --'Promo'
    ELSE '0'
    END AS POSDiscTypeCode
  -- Measures
  , - TSE.Quantity AS Quantity
  , TSE.Price * - (TSE.Quantity) AS GrossAmountInclVAT_PCY
  , - TSE.[Cost Amount] AS CostAmount_PCY
  , TSE.[VAT Amount] AS VATAmount_PCY
  , TSE.[Net Amount] + TSE.[VAT Amount] AS NetSalesAmountInclVAT_PCY
  , - TSE.[Net Amount] AS NetSalesAmount_PCY
  , TSE.[Discount Amount] + TSE.[Total Discount] AS DiscountAmount_PCY
  , TSE.[Customer Discount] AS CustomerDiscountAmount_PCY
  , TSE.[Total Discount] AS TotalDiscountAmount_PCY
  , TSE.[Line Discount] AS LineDiscountAmount_PCY
  , TSE.[Infocode Discount] AS InfoCodeDiscountAmount_PCY
  , TSE.[Cust_ Invoice Discount] AS CustomerInvoiceDiscountAmount_PCY
  , TSE.[Periodic Discount] AS PeriodicDiscountAmount_PCY
  , TSE.Price * - (TSE.Quantity) * ER.CrossRate AS GrossAmountInclVAT_RCY
  , - TSE.[Cost Amount] * ER.CrossRate AS CostAmount_RCY
  , TSE.[VAT Amount] * ER.CrossRate AS VATAmount_RCY
  , TSE.[Net Amount] + TSE.[VAT Amount] * ER.CrossRate AS NetSalesAmountInclVAT_RCY
  , - TSE.[Net Amount] * ER.CrossRate AS NetSalesAmount_RCY
  , TSE.[Discount Amount] + TSE.[Total Discount] * ER.CrossRate AS DiscountAmount_RCY
  , TSE.[Customer Discount] * ER.CrossRate AS CustomerDiscountAmount_RCY
  , TSE.[Total Discount] * ER.CrossRate AS TotalDiscountAmount_RCY
  , TSE.[Line Discount] * ER.CrossRate AS LineDiscountAmount_RCY
  , TSE.[Infocode Discount] * ER.CrossRate AS InfoCodeDiscountAmount_RCY
  , TSE.[Cust_ Invoice Discount] * ER.CrossRate AS CustomerInvoiceDiscountAmount_RCY
  , TSE.[Periodic Discount] * ER.CrossRate AS PeriodicDiscountAmount_RCY
FROM stage_nav.[Trans_ Sales Entry] AS TSE
LEFT JOIN help.ExchangeRates AS ER
  ON TSE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = TSE.company_id
    AND ER.DataConnectionID = TSE.data_connection_id
INNER JOIN stage_nav.[Transaction Header] AS TH
  ON TH.[Store No_] = TSE.[Store No_]
    AND TH.[POS Terminal No_] = TSE.[POS Terminal No_]
    AND TH.[Transaction No_] = TSE.[Transaction No_]
    AND TH.company_id = TSE.company_id
    AND TH.data_connection_id = TSE.data_connection_id
LEFT JOIN stage_nav.[Trans_ Sales Entry Status] AS SES
  ON SES.[Store No_] = TSE.[Store No_]
    AND SES.[POS Terminal No_] = TSE.[POS Terminal No_]
    AND SES.[Transaction No_] = TSE.[Transaction No_]
    AND SES.[Line No_] = TSE.[Line No_]
    AND SES.company_id = TSE.company_id
    AND SES.data_connection_id = TSE.data_connection_id
