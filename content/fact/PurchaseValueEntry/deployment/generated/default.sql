SELECT
  -- system information
  VE.stage_id AS StageID
  , VE.Company_Id AS CompanyID
  , VE.data_connection_id AS DataConnectionID
  , 1 AS PurchaseValueEntryType
  -- business key
  , VE.[Document No_] AS DocumentNo
  , PVE.[Type] AS PurchaseLineTypeCode
  , VE.[Item Ledger Entry No_] AS ItemLedgerEntryNo
  , VE.[Entry No_] AS ValueEntryNo
  -- Other fields
  , VE.[Source Type] AS SourceType
  , VE.[Source Posting Group] AS SourcePostingGroup
  -- dimensions                                                                       
  , VE.[Item No_] AS ItemCode
  , CAST(NULL AS NVARCHAR(20)) AS GeneralLedgerAccountCode
  , CAST(NULL AS NVARCHAR(20)) AS ResourceCode
  , CAST(NULL AS NVARCHAR(20)) AS FixedAssetCode
  , VE.[Item Charge No_] AS ItemChargeCode
  , PVE.DocumentPostingDate AS DocumentPostingDate
  , VE.[Posting Date] AS PostingDate
  , PVE.BuyFromVendorNo AS BuyFromVendorCode
  , PVE.PayToVendorNo AS PayToVendorCode
  , PVE.DocumentType AS DocumentTypeCode
  , VE.[Location Code] AS LocationCode
  , VE.[Variant Code] AS ItemVariantCode
  , VE.[Source Code] AS SourceCode
  , VE.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , VE.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , VE.[Reason Code] AS ReasonCode
  , VE.[Salespers__Purch_ Code] AS SalesPersonPurchaserCode
  , PVE.CurrencyCode AS CurrencyCode
  , VE.[Inventory Posting Group] AS InventoryPostingGroupCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , VE.[Invoiced Quantity] AS InvoicedQuantity
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Cost Amount (actual)]
    ELSE 0
    END AS PurchaseAmount_LCY
  , VE.[Cost Amount (actual)] + VE.[Cost Amount (expected)] AS PurchaseAmountExpected_LCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Discount Amount]
    ELSE 0
    END AS DiscountAmount_LCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Posted To G_L]
    ELSE 0
    END AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Cost Amount (actual)] * ER.CrossRate
    ELSE 0
    END AS PurchaseAmount_RCY
  , (VE.[Cost Amount (actual)] + VE.[Cost Amount (expected)]) * ER.CrossRate AS PurchaseAmountExpected_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Discount Amount] * ER.CrossRate
    ELSE 0
    END AS DiscountAmount_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Posted To G_L] * ER.CrossRate
    ELSE 0
    END AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Cost Amount (actual)] * PVE.CurrencyFactor
    ELSE 0
    END AS PurchaseAmount_PCY
  , (VE.[Cost Amount (actual)] + VE.[Cost Amount (expected)]) * PVE.CurrencyFactor AS PurchaseAmountExpected_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Discount Amount] * PVE.CurrencyFactor
    ELSE 0
    END AS DiscountAmount_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Posted To G_L] * PVE.CurrencyFactor
    ELSE 0
    END AS CostAmountPostedToGL_PCY
FROM stage_nav.[Value Entry] AS VE
INNER JOIN help.[PurchaseValueEntry] AS PVE
  ON VE.[Entry No_] = PVE.EntryNo
    AND VE.Company_Id = PVE.Company_ID
    AND VE.Data_connection_id = PVE.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON VE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = VE.company_id
    AND ER.DataConnectionID = VE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON VE.[Dimension Set ID] = FD.dimensionsetno
    AND VE.company_id = FD.companyid
    AND VE.data_connection_id = FD.DataConnectionID
WHERE VE.[Item Ledger Entry Type] = 0 --Purchase
