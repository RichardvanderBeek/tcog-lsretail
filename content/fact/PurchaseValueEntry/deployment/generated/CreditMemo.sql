SELECT
  -- system information
  PCRML.stage_id AS StageID
  , PCRML.Company_Id AS CompanyID
  , PCRML.data_connection_id AS DataConnectionID
  , 3 AS PurchaseValueEntryType
  -- business key
  , PCRML.[Document No_] AS DocumentNo
  , PCRML.[Type] AS PurchaseLineTypeCode
  , 0 AS ItemLedgerEntryNo
  , 0 AS ValueEntryNo
  -- Other fields
  , 0 AS SourceType
  , cast(NULL AS NVARCHAR(20)) AS SourcePostingGroup
  -- dimensions                                                                       
  , cast(NULL AS NVARCHAR(20)) AS ItemCode
  , CASE 
    WHEN PCRML.[Type] = 1
      THEN PCRML.[No_]
    ELSE ''
    END AS GeneralLedgerAccountCode
  , CASE 
    WHEN PCRML.[Type] = 3
      THEN PCRML.[No_]
    ELSE ''
    END AS ResourceCode
  , CASE 
    WHEN PCRML.[Type] = 4
      THEN PCRML.[No_]
    ELSE ''
    END AS FixedAssetCode
  , CASE 
    WHEN PCRML.[Type] = 5
      THEN PCRML.[No_]
    ELSE ''
    END AS ItemChargeCode
  , PCRMH.[Posting Date] AS DocumentPostingDate
  , PCRML.[Posting Date] AS PostingDate
  , PCRMH.[Buy-from Vendor No_] AS BuyFromVendorCode
  , PCRMH.[Pay-to Vendor No_] AS PayToVendorCode
  , 8 AS DocumentTypeCode --Purchase Credit Memo
  , PCRML.[Location Code] AS LocationCode
  , cast(NULL AS NVARCHAR(20)) AS ItemVariantCode
  , PCRMH.[Source Code] AS SourceCode
  , PCRML.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , PCRML.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , PCRMH.[Reason Code] AS ReasonCode
  , PCRMH.[Purchaser Code] AS SalesPersonPurchaserCode
  , PCRMH.[Currency Code] AS CurrencyCode
  , cast(NULL AS NVARCHAR(20)) AS InventoryPostingGroupCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , PCRML.[Quantity] * - 1 AS InvoicedQuantity
  , PCRML.[Unit Cost (LCY)] * PCRML.Quantity * - 1 AS PurchaseAmount_LCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS PurchaseAmountExpected_LCY
  , CASE 
    WHEN PCRMH.[Currency Factor] <> 0
      THEN PCRML.[Line Discount Amount] / PCRMH.[Currency Factor]
    ELSE PCRML.[Line Discount Amount]
    END AS DiscountAmount_LCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , PCRML.[Unit Cost (LCY)] * PCRML.Quantity * ER.CrossRate * - 1 AS PurchaseAmount_RCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS PurchaseAmountExpected_RCY
  , CASE 
    WHEN PCRMH.[Currency Factor] <> 0
      THEN (PCRML.[Line Discount Amount] / PCRMH.[Currency Factor]) * ER.CrossRate * - 1
    ELSE PCRML.[Line Discount Amount] * ER.CrossRate * - 1
    END AS DiscountAmount_RCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , PCRML.[Unit Cost (LCY)] * PCRML.Quantity * - 1 AS PurchaseAmount_PCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS PurchaseAmountExpected_PCY
  , CASE 
    WHEN PCRMH.[Currency Factor] <> 0
      THEN (PCRML.[Line Discount Amount] / PCRMH.[Currency Factor]) * - 1
    ELSE PCRML.[Line Discount Amount] * - 1
    END AS DiscountAmount_PCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS CostAmountPostedToGL_PCY
FROM stage_nav.[Purch_ Cr_ Memo Line] AS PCRML
LEFT JOIN stage_nav.[Purch_ Cr_ Memo Hdr_] AS PCRMH
  ON PCRML.[Document No_] = PCRMH.[No_]
    AND PCRML.Company_Id = PCRMH.Company_ID
    AND PCRML.Data_connection_id = PCRMH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON PCRML.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PCRML.company_id
    AND ER.DataConnectionID = PCRML.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON PCRML.[Dimension Set ID] = FD.dimensionsetno
    AND PCRML.company_id = FD.companyid
    AND PCRML.data_connection_id = FD.DataConnectionID
WHERE PCRML.Type NOT IN (
    2
    , 6
    ) -- Item, Itemcharge
