SELECT
  -- System Information
  TIE.stage_id AS StageID
  , TIE.Company_Id AS CompanyID
  , TIE.data_connection_id AS DataConnectionID
  -- Dimensions
  , TIE.[Store No_] AS StoreCode
  , TIE.[POS Terminal No_] AS POSTerminalCode
  , TIE.[Transaction No_] AS TransactionCode
  , TIE.[Staff ID] AS StaffCode
  , TIE.[Date] AS PostingDate
  , CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, TIE.TIME), 0)), 108) AS PostingTime
  , TIE.[Infocode] AS InfoCode
  , TIE.[Subcode] AS SubCode
  -- Measures            
  , TIE.[Info_ Amt_] AS InfoAmount_PCY
  , TIE.[Amount] AS Amount_PCY
  , TIE.[Info_ Amt_] * ER.Crossrate AS InfoAmount_RCY
  , TIE.[Amount] * ER.Crossrate AS Amount_RCY
FROM stage_nav.[Trans_ Infocode Entry] AS TIE
LEFT JOIN help.ExchangeRates AS ER
  ON TIE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = TIE.company_id
    AND ER.DataConnectionID = TIE.data_connection_id
