WITH OpenAmountVoucherEntries_CTE
AS (
  SELECT VE.stage_id AS StageID
    , VE.Company_Id AS CompanyID
    , VE.data_connection_id AS DataConnectionID
    , VE.[Voucher No_] AS VoucherCode
    , SUM(VE.Amount) AS OpenAmount
  FROM stage_nav.[Voucher Entries] VE
  GROUP BY VE.[Voucher No_]
    , VE.company_id
    , VE.data_connection_id
    , VE.stage_id
    , VE.Voided
  HAVING VE.Voided = 0
  )
  , IssuedAmountVoucherEntries_CTE
AS (
  SELECT VE.stage_id AS StageID
    , VE.Company_Id AS CompanyID
    , VE.data_connection_id AS DataConnectionID
    , VE.[Voucher No_] AS VoucherCode
    , SUM(VE.Amount) AS IssuedAmount
  FROM stage_nav.[Voucher Entries] VE
  GROUP BY VE.[Voucher No_]
    , VE.company_id
    , VE.data_connection_id
    , VE.stage_id
    , VE.Voided
    , VE.[Entry Type]
  HAVING VE.Voided = 0
    AND VE.[Entry Type] = 0
  )
  , RedeemedAmountVoucherEntries_CTE
AS (
  SELECT VE.stage_id AS StageID
    , VE.Company_Id AS CompanyID
    , VE.data_connection_id AS DataConnectionID
    , VE.[Voucher No_] AS VoucherCode
    , SUM(VE.Amount) AS RedeemedAmount
  FROM stage_nav.[Voucher Entries] VE
  GROUP BY VE.[Voucher No_]
    , VE.company_id
    , VE.data_connection_id
    , VE.stage_id
    , VE.Voided
    , VE.[Entry Type]
  HAVING VE.Voided = 0
    AND VE.[Entry Type] = 1
  )
SELECT
  -- System Information
  PDE.stage_id AS StageID
  , PDE.Company_Id AS CompanyID
  , PDE.data_connection_id AS DataConnectionID
  -- Dimensions
  , PDE.[Entry Type] AS POSDataEntryType
  , PDE.[Entry Code] AS POSDataEntryCode
  , PDE.[Created by Receipt No_] AS CreatedByReceiptCode
  , PDE.[Applied by Receipt No_] AS AppliedByReceiptCode
  , PDE.[Reserverd By POS No_] AS POSTerminalCode
  , PDE.[Date Created] AS CreatedDate
  , PDE.[Date Applied] AS AppliedDate
  , PDE.[Created in Store No_] AS StoreCode
  , PDE.[Contact No_] AS StaffCode
  , PDE.[Expiring Date] AS ExpiringDate
  -- Measures
  , CASE DET.[Create Voucher Entry]
    WHEN 0
      THEN ISNULL((PDE.[Amount] - PDE.[Applied Amount]), 0)
    WHEN 1
      THEN ISNULL(OA.OpenAmount, 0)
    END AS OpenAmount_PCY
  , CASE DET.[Create Voucher Entry]
    WHEN 0
      THEN ISNULL(PDE.Amount, 0)
    WHEN 1
      THEN ISNULL(IA.IssuedAmount, 0)
    END AS IssuedAmount_PCY
  , CASE DET.[Create Voucher Entry]
    WHEN 0
      THEN ISNULL(PDE.[Applied Amount], 0)
    WHEN 1
      THEN ISNULL(RA.RedeemedAmount, 0)
    END AS RedeemedAmount_PCY
  , CASE DET.[Create Voucher Entry]
    WHEN 0
      THEN ISNULL((PDE.[Amount] * ER.CrossRate - PDE.[Applied Amount] * ER.CrossRate), 0)
    WHEN 1
      THEN ISNULL(OA.[OpenAmount] * ER.CrossRate, 0)
    END AS OpenAmount_RCY
  , CASE DET.[Create Voucher Entry]
    WHEN 0
      THEN ISNULL(PDE.Amount * ER.CrossRate, 0)
    WHEN 1
      THEN ISNULL(IA.IssuedAmount * ER.CrossRate, 0)
    END AS IssuedAmount_RCY
  , CASE DET.[Create Voucher Entry]
    WHEN 0
      THEN ISNULL(PDE.[Applied Amount] * ER.CrossRate * ER.CrossRate, 0)
    WHEN 1
      THEN ISNULL(RA.[RedeemedAmount] * ER.CrossRate * ER.CrossRate, 0)
    END AS RedeemedAmount_RCY
FROM stage_nav.[POS Data Entry] AS PDE
LEFT JOIN help.ExchangeRates AS ER
  ON PDE.[Date Applied] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PDE.company_id
    AND ER.DataConnectionID = PDE.data_connection_id
LEFT JOIN stage_nav.[POS Data Entry Type] AS DET
  ON DET.Code = PDE.[Entry Type]
    AND DET.company_id = PDE.Company_Id
    AND DET.data_connection_id = PDE.Data_Connection_Id
LEFT JOIN OpenAmountVoucherEntries_CTE AS OA
  ON OA.VoucherCode = PDE.[Entry Code]
    AND OA.CompanyId = PDE.Company_Id
    AND OA.DataConnectionId = PDE.Data_Connection_Id
LEFT JOIN IssuedAmountVoucherEntries_CTE AS IA
  ON IA.VoucherCode = PDE.[Entry Code]
    AND IA.CompanyId = PDE.Company_Id
    AND IA.DataConnectionId = PDE.Data_Connection_Id
LEFT JOIN RedeemedAmountVoucherEntries_CTE AS RA
  ON RA.VoucherCode = PDE.[Entry Code]
    AND RA.CompanyId = PDE.Company_Id
    AND RA.DataConnectionId = PDE.Data_Connection_Id
