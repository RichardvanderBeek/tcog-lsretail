SELECT
  -- System Information
  PSL.stage_id AS StageID
  , PSL.Company_Id AS CompanyID
  , PSL.data_connection_id AS DataConnectionID
  -- Dimensions
  , PS.[No_] AS POSStatementCode
  , PS.DATE AS PostedStatementDate
  , PS.[Posting Date] AS PostingDate
  , PS.[Posted Date] AS PostedDate
  , PS.[Calculated Date] AS CalculatedDate
  , PSL.[Store No_] AS StoreCode
  , PSL.[POS Terminal No_] AS POSTerminalCode
  , PSL.[Staff ID] AS StaffCode
  , 'N/A' AS CardCode
  , PSL.[Tender Type] AS TenderTypeCode
  , PSL.[Currency Code] AS CurrencyCode
  , PSL.[Bank Bag No_] AS BankBagCode
  , PSL.[Bal_ Account No_] AS BalanceSafeAccountCode
  , PSL.[Line No_] AS StatementLineNo
  -- Measures
  , PSL.[Amount] AS CountedSafeAmount_PCY
  , PSL.[Trans_ Amount] AS TransSafeAmount_PCY
  , PSL.[Difference Amount] AS DifferenceSafeAmount_PCY
  , PSL.[Amount] * ER.CrossRate AS CountedSafeAmount_RCY
  , PSL.[Trans_ Amount] * ER.CrossRate AS TransSafeAmount_RCY
  , PSL.[Difference Amount] * ER.CrossRate AS DifferenceSafeAmount_RCY
  , PSL.[Amount in LCY] AS CountedSafeAmount_LCY
  , PSL.[Trans_ Amount in LCY] AS TransSafeAmount_LCY
  , PSL.[Difference in LCY] AS DifferenceSafeAmount_LCY
FROM stage_nav.[Posted Safe Statement Line] AS PSL
INNER JOIN stage_nav.[Posted Statement] AS PS
  ON PS.[No_] = PSL.[Statement No_]
    AND PS.company_id = PSL.company_id
    AND PS.data_connection_id = PSL.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON PS.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PS.company_id
    AND ER.DataConnectionID = PS.data_connection_id
LEFT JOIN help.Enumerations AS e_BAT
  ON e_BAT.TableName = 'Posted Safe Statement Line'
    AND e_BAT.ColumnName = 'Bal. Account Type'
    AND e_BAT.OptionNo = PSL.[Bal_ Account No_]
    AND e_BAT.DataConnectionId = PSL.Data_Connection_Id
