SELECT
  -- System Information
  TIE.stage_id AS StageID
  , TIE.Company_Id AS CompanyID
  , TIE.data_connection_id AS DataConnectionID
  -- Dimensions
  , TIE.[No_] AS IncomeExpenseCode
  , TIE.[Store No_] AS StoreCode
  , TIE.[POS Terminal No_] AS POSTerminalCode
  , TIE.[Transaction No_] AS TransactionCode
  , TIE.[Line No_] AS LineCode
  , TIE.[Account Type] AS AccountTypeCode
  , TIE.[Staff ID] AS StaffCode
  , TIE.[Date] AS PostingDate
  , CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, TIE.TIME), 0)), 108) AS PostingTime
  -- Measures
  , CASE TIE.[Account Type]
    WHEN 0
      THEN TIE.Amount
    ELSE 0
    END AS IncomeAmount_PCY
  , CASE TIE.[Account Type]
    WHEN 1
      THEN TIE.Amount
    ELSE 0
    END AS ExpenseAmount_PCY
  , CASE TIE.[Account Type]
    WHEN 2
      THEN TIE.Amount
    ELSE 0
    END AS SuspenseAmount_PCY
  , CASE TIE.[Account Type]
    WHEN 0
      THEN TIE.Amount * ER.Crossrate
    ELSE 0
    END AS IncomeAmount_RCY
  , CASE TIE.[Account Type]
    WHEN 1
      THEN TIE.Amount * ER.Crossrate
    ELSE 0
    END AS ExpenseAmount_RCY
  , CASE TIE.[Account Type]
    WHEN 2
      THEN TIE.Amount * ER.Crossrate
    ELSE 0
    END AS SuspenseAmount_RCY
FROM stage_nav.[Trans_ Income_Expense Entry] AS TIE
LEFT JOIN help.ExchangeRates AS ER
  ON TIE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = TIE.company_id
    AND ER.DataConnectionID = TIE.data_connection_id
