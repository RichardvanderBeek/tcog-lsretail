SELECT
  -- System Information
  VTL.stage_id AS StageID
  , VTL.Company_Id AS CompanyID
  , VTL.data_connection_id AS DataConnectionID
  -- Dimensions
  , VT.[Receipt No_] AS ReceiptCode
  , VT.[Store No_] AS StoreCode
  , VT.[POS Terminal No_] AS POSTerminalCode
  , VT.[Created on POS Terminal] AS CreatedOnPOSTerminalCode
  , VT.[Staff ID] AS StaffCode
  , VT.[Sales Type] AS SalesTypeCode
  , VT.[Trans_ Date] AS ReceiptDate
  , CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, VT.[Trans_ Time]), 0)), 108) AS ReceiptTime
  , VT.[Customer No_] AS CustomerCode
  , VTL.[Entry Status] AS EntryStatusCode
  , VTL.[Number] AS ItemCode
  , VTL.[Variant Code] AS ItemVariantCode
  , VTL.[Sales Staff] AS SalesStaffCode
  -- Measures   
  , VTL.Quantity AS Quantity
  , CASE 
    WHEN (VT.[Sale Is Return Sale] = 1)
      OR (VTL.Quantity > 0)
      THEN 1
    ELSE 0
    END AS IsReturnSale
  , CASE VTL.[Entry Type]
    WHEN 0
      THEN VTL.Amount
    ELSE 0
    END AS VoidedGrossAmountItem_PCY
  , CASE VTL.[Entry Type]
    WHEN 1
      THEN VTL.Amount
    ELSE 0
    END AS VoidedGrossAmountPayment_PCY
  , CASE VTL.[Entry Type]
    WHEN 2
      THEN VTL.Amount
    ELSE 0
    END AS VoidedGrossAmountSpecialDisc_PCY
  , CASE VTL.[Entry Type]
    WHEN 3
      THEN VTL.Amount
    ELSE 0
    END AS VoidedGrossAmountTotalDisc_PCY
  , CASE VTL.[Entry Type]
    WHEN 4
      THEN VTL.Amount
    ELSE 0
    END AS VoidedGrossAmountIncomeExpense_PCY
  , VTL.[Net Amount] AS VoidedNetAmount_PCY
  , VTL.[VAT Amount] AS VoidedVatAmount_PCY
  , VTL.[Net Amount] + VTL.[VAT Amount] AS VoidedNetAmountInclVat_PCY
  , VTL.[Cost Amount] AS VoidedCostAmount_PCY
  , VTL.[Discount Amount] AS VoidedDiscountAmount_PCY
  , CASE VTL.[Entry Type]
    WHEN 0
      THEN VTL.Amount * ER.Crossrate
    ELSE 0
    END AS VoidedGrossAmountItem_RCY
  , CASE VTL.[Entry Type]
    WHEN 1
      THEN VTL.Amount * ER.Crossrate
    ELSE 0
    END AS VoidedGrossAmountPayment_RCY
  , CASE VTL.[Entry Type]
    WHEN 2
      THEN VTL.Amount * ER.Crossrate
    ELSE 0
    END AS VoidedGrossAmountSpecialDisc_RCY
  , CASE VTL.[Entry Type]
    WHEN 3
      THEN VTL.Amount * ER.Crossrate
    ELSE 0
    END AS VoidedGrossAmountTotalDisc_RCY
  , CASE VTL.[Entry Type]
    WHEN 4
      THEN VTL.Amount * ER.Crossrate
    ELSE 0
    END AS VoidedGrossAmountIncomeExpense_RCY
  , VTL.[Net Amount] * ER.Crossrate AS VoidedNetAmount_RCY
  , VTL.[VAT Amount] * ER.Crossrate AS VoidedVatAmount_RCY
  , VTL.[Net Amount] + VTL.[VAT Amount] * ER.Crossrate AS VoidedNetAmountInclVat_RCY
  , VTL.[Cost Amount] * ER.Crossrate AS VoidedCostAmount_RCY
  , VTL.[Discount Amount] * ER.Crossrate AS VoidedDiscountAmount_RCY
FROM stage_nav.[POS Voided Trans_ Line] AS VTL
LEFT JOIN stage_nav.[POS Voided Transaction] AS VT
  ON VT.[Receipt No_] = VTL.[Receipt No_]
    AND VT.company_id = VTL.company_id
    AND VT.data_connection_id = VTL.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON VT.[Trans_ Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = VTL.company_id
    AND ER.DataConnectionID = VTL.data_connection_id
