SELECT
  -- system information
  TL.stage_id AS StageID
  , TL.company_id AS CompanyID
  , TL.data_connection_id AS DataConnectionID
  -- dimensions                                                                       
  , TL.[Document No_] AS DocumentTransferCode
  , COALESCE(NULLIF(TL.[Transfer-from Code], ''), TH.[Transfer-from Code]) AS TransferFromLocationCode
  , COALESCE(NULLIF(TL.[Transfer-to Code], ''), TH.[Transfer-to Code]) AS TransferToLocationCode
  , COALESCE(NULLIF(TL.[In-Transit Code], ''), TH.[In-Transit Code]) AS InTransitLocationCode
  , TH.[Posting Date] AS PostingDate
  , COALESCE(NULLIF(TL.[Shipment Date], '1753-01-01 00:00:00'), NULLIF(TH.[Shipment Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS ShipmentDate
  , COALESCE(NULLIF(TL.[Receipt Date], '1753-01-01 00:00:00'), NULLIF(TH.[Receipt Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS ReceiptDate
  , TH.[Shipment Method Code] AS ShipmentMethodCode
  , TH.[Transport Method] AS TransportMethodCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , TL.[Item No_] AS ItemCode
  , TL.[Variant Code] AS ItemVariantCode
  , TL.[Inventory Posting Group] AS InventoryPostingGroupCode
  , TL.[Transfer-from Bin Code] AS TransferFromBinCode
  , TL.[Transfer-To Bin Code] AS TransferToBinCode
  , TL.[Unit of Measure Code] AS UnitOfMeasureCode
  , TH.[Assigned User ID] AS WarehouseEmployeeCode
  -- measures
  , TL.Quantity AS TransferOrderQuantity
  , TL.[Outstanding Quantity] AS TransferOrderOutstandingQuantity
  , TL.[Qty_ to Ship] AS TransferOrderQuantityToShip
  , TL.[Qty_ to Receive] AS TransferOrderQuantityToReceive
  , TL.[Quantity Shipped] AS TransferOrderQuantityShipped
  , TL.[Quantity Received] AS TransferOrderQuantityReceived
  , TL.[Qty_ in Transit] AS TransferOrderQuantityInTransit
  , TL.[Quantity (Base)] AS TransferOrderQuantityBase
  , TL.[Outstanding Qty_ (Base)] AS TransferOrderOutstandingQuantityBase
  , TL.[Qty_ to Ship (Base)] AS TransferOrderQuantityToShipBase
  , TL.[Qty_ to Receive (Base)] AS TransferOrderQuantityToReceiveBase
  , TL.[Qty_ Shipped (Base)] AS TransferOrderQuantityShippedBase
  , TL.[Qty_ Received (Base)] AS TransferOrderQuantityReceivedBase
  , TL.[Qty_ in Transit (Base)] AS TransferOrderQuantityInTransitBase
FROM stage_nav.[Transfer Line] AS TL
INNER JOIN stage_nav.[Transfer Header] AS TH
  ON TH.No_ = TL.[Document No_]
    AND TL.company_id = TH.company_id
    AND TL.data_connection_id = TH.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON FD.DimensionSetNo = COALESCE(NULLIF(TL.[Dimension Set ID], 0), TH.[Dimension Set ID])
    AND FD.CompanyID = TL.company_id
    AND FD.DataConnectionID = TL.data_connection_id
WHERE TL.[Derived From Line No_] = 0
