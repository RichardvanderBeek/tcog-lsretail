SELECT
  -- system information
  GLE.stage_id AS StageID
  , GLE.Company_Id AS CompanyID
  , GLE.data_connection_id AS DataConnectionID
  -- dimensions
  , GLE.[Entry No_] AS EntryNo
  , GLE.[G_L Account No_] AS GeneralLedgerAccountCode
  , GLE.[Document Type] AS DocumentTypeCode
  , GLE.[Document No_] AS DocumentCode
  , CAST(GLE.[Document Date] AS DATE) AS DocumentDate
  , GLE.[Source Type] AS SourceTypeCode
  , GLE.[Source Code] AS SourceCode
  , GLE.[Reason Code] AS ReasonCode
  , GLE.[Gen_ Posting Type] AS GeneralPostingTypeCode
  , GLE.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , GLE.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , GLE.[Business Unit Code] AS BusinessUnitCode
  , CAST(GLE.[Posting Date] AS DATE) AS PostingDate
  , CASE 
    WHEN DATEPART(hh, GLE.[Posting Date]) <> 0
      THEN 1
    ELSE 0
    END AS IsPeriodClosingCode
  , GLA.Income_Balance AS IsBalanceCode
  , CASE 
    WHEN GLE.[Credit Amount] <> 0
      THEN 1
    ELSE 0
    END AS IsCreditCode
  , COALESCE(CASE 
      WHEN DATEPART(hh, GLE.[Posting Date]) <> 0
        THEN 1
      ELSE 0
      END, - 1) AS IsPeriodClosing
  , COALESCE(GLA.Income_Balance, - 1) AS IsBalance
  , COALESCE(CASE 
      WHEN GLE.[Credit Amount] <> 0
        THEN 1
      ELSE 0
      END, - 1) AS IsCredit
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , CASE 
    WHEN GLE.[Source Type] = 1
      THEN GLE.[Source No_]
    ELSE NULL
    END AS CustomerCode
  , CASE 
    WHEN GLE.[Source Type] = 2
      THEN GLE.[Source No_]
    ELSE NULL
    END AS VendorCode
  , CASE 
    WHEN GLE.[Source Type] = 4
      THEN GLE.[Source No_]
    ELSE NULL
    END AS FixedAssetCode
  -- measures
  , GLE.[Amount] AS Amount_LCY
  , GLE.[Debit Amount] AS AmountDebit_LCY
  , GLE.[Credit Amount] AS AmountCredit_LCY
  , GLE.[VAT Amount] AS AmountVAT_LCY
  -- measures_RCY
  , GLE.[Amount] * ER.CrossRate AS Amount_RCY
  , GLE.[Debit Amount] * ER.CrossRate AS AmountDebit_RCY
  , GLE.[Credit Amount] * ER.CrossRate AS AmountCredit_RCY
FROM stage_nav.[G_L Entry] AS GLE
LEFT JOIN stage_nav.[G_L Account] AS GLA
  ON GLA.No_ = GLE.[G_L Account No_]
    AND GLA.Company_Id = GLE.Company_Id
    AND GLA.Data_Connection_Id = GLE.Data_Connection_Id
LEFT JOIN help.ExchangeRates AS ER
  ON GLE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = GLE.company_id
    AND ER.DataConnectionID = GLE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON GLE.[Dimension Set ID] = FD.DimensionSetNo
    AND GLE.company_id = FD.CompanyID
    AND GLE.data_connection_id = FD.DataConnectionID
