SELECT
  -- System Information
  PSL.stage_id AS StageID
  , PSL.Company_Id AS CompanyID
  , PSL.data_connection_id AS DataConnectionID
  -- Dimensions
  , PS.[No_] AS POSStatementCode
  , PS.DATE AS PostedStatementDate
  , PS.[Posting Date] AS PostingDate
  , PS.[Posted Date] AS PostedDate
  , PS.[Calculated Date] AS CalculatedDate
  , PSL.[Store No_] AS StoreCode
  , PSL.[POS Terminal No_] AS POSTerminalCode
  , PSL.[Staff ID] AS StaffCode
  , PSL.[Tender Type] AS TenderTypeCode
  , PSL.[Tender Type Card No_] AS CardCode
  , PSL.[Currency Code] AS CurrencyCode
  , PSL.[Line No_] AS StatementLineNo
  -- Measures PCY
  , PSL.[Added to Drawer] AS AddedtoDrawer_PCY
  , PSL.[Removed from Drawer] AS RemovedfromDrawer_PCY
  , PSL.[Change Tender] AS ChangeTender_PCY
  , PSL.[In Drawer at End of Day] AS InDraweratEndofDay_PCY
  , PSL.[Difference Amount] AS DifferenceAmount_PCY
  , PSL.[Trans_ Amount] AS TransAmount_PCY
  -- RCY
  , PSL.[Added to Drawer] * ER.CrossRate AS AddedtoDrawer_RCY
  , PSL.[Removed from Drawer] * ER.CrossRate AS RemovedfromDrawer_RCY
  , PSL.[Change Tender] * ER.CrossRate AS ChangeTender_RCY
  , PSL.[In Drawer at End of Day] * ER.CrossRate AS InDraweratEndofDay_RCY
  , PSL.[Difference Amount] * ER.CrossRate AS DifferenceAmount_RCY
  , PSL.[Trans_ Amount] * ER.CrossRate AS TransAmount_RCY
  -- LCY
  , PSL.[Trans_ Amount in LCY] AS TransAmount_LCY
  , PSL.[Difference in LCY] AS DifferenceAmount_LCY
FROM stage_nav.[Posted Statement Line] AS PSL
INNER JOIN stage_nav.[Posted Statement] AS PS
  ON PS.[No_] = PSL.[Statement No_]
    AND PS.company_id = PSL.company_id
    AND PS.data_connection_id = PSL.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON PS.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PS.company_id
    AND ER.DataConnectionID = PS.data_connection_id
