SELECT
  -- System Information
  TSE.stage_id AS StageID
  , TSE.Company_Id AS CompanyID
  , TSE.data_connection_id AS DataConnectionID
  -- Dimensions
  , TSE.[Store No_] AS StoreCode
  , TSE.[POS Terminal No_] AS POSTerminalCode
  , TSE.[Transaction No_] AS TransactionCode
  , TSE.[Sales Staff] AS SalesPersonCode
  , TSE.[Staff ID] AS StaffCode
  , TSE.[Date] AS ReceiptDate
  , CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, TSE.TIME), 0)), 108) AS ReceiptTime
  -- Measures
  , TH.[Sale Is Return Sale] AS IsReturnSale
  , TSE.[Item Number Scanned] AS ItemNumberScanned
  , TSE.[Keyboard Item Entry] AS KeyboardItemEntry
  , TSE.[Price in Barcode] AS PriceBarcode
  , TSE.[Weight Manually Entered] AS WeightManuallyEntered
  , TSE.[Scale Item] AS ScaleItem
  , TSE.[Weight Item] AS WeightItem
  , TSE.[Serial_Lot No_ Not Valid] AS SerialLotNoNotValid
  , TSE.[Price Change] AS PriceChange
  , TSE.[Line was Discounted] AS LineWasDiscounted
  , TSE.[Deal Line] AS DealLine
FROM stage_nav.[Trans_ Sales Entry] AS TSE
LEFT JOIN help.ExchangeRates AS ER
  ON TSE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = TSE.company_id
    AND ER.DataConnectionID = TSE.data_connection_id
LEFT JOIN stage_nav.[Transaction Header] AS TH
  ON TH.[Store No_] = TSE.[Store No_]
    AND TH.[POS Terminal No_] = TSE.[POS Terminal No_]
    AND TH.[Transaction No_] = TSE.[Transaction No_]
    AND TH.company_id = TSE.company_id
    AND TH.data_connection_id = TSE.data_connection_id
