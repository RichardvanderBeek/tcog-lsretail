SELECT
  -- System Information
  TPE.stage_id AS StageID
  , TPE.Company_Id AS CompanyID
  , TPE.data_connection_id AS DataConnectionID
  -- Dimensions
  , TPE.[Store No_] AS StoreCode
  , TPE.[POS Terminal No_] AS POSTerminalCode
  , TPE.[Transaction No_] AS TransactionCode
  , TPE.[Staff ID] AS StaffCode
  , TPE.[Date] AS PostingDate
  , CONVERT(NVARCHAR(40), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, TPE.TIME), 0)), 108) AS TimeStrSecond
  , TPE.[Tender Type] AS TenderTypeCode
  , TPE.[Card No_] AS CardCode
  , TPE.[Currency Code] AS CurrencyCode
  -- Measures            
  , CASE 
    WHEN TPE.[Change Line] = 0
      THEN ISNULL(TPE.[Amount Tendered], 0)
    ELSE 0
    END AS PaidAmount_PCY
  , CASE 
    WHEN TPE.[Change Line] = 0
      THEN ISNULL(TPE.[Amount in Currency], 0)
    ELSE 0
    END AS PaidAmountCurrency_PCY
  , CASE 
    WHEN TPE.[Change Line] = 1
      THEN ISNULL(TPE.[Amount Tendered], 0)
    ELSE 0
    END AS ChangeAmount_PCY
  , CASE 
    WHEN TPE.[Change Line] = 0
      THEN ISNULL(TPE.[Amount Tendered], 0) * ER.CrossRate
    ELSE 0
    END AS PaidAmount_RCY
  , CASE 
    WHEN TPE.[Change Line] = 0
      THEN ISNULL(TPE.[Amount in Currency], 0) * ER.CrossRate
    ELSE 0
    END AS PaidAmountCurrency_RCY
  , CASE 
    WHEN TPE.[Change Line] = 1
      THEN ISNULL(TPE.[Amount Tendered], 0) * ER.CrossRate
    ELSE 0
    END AS ChangeAmount_RCY
FROM stage_nav.[Trans_ Payment Entry] AS TPE
LEFT JOIN help.ExchangeRates AS ER
  ON TPE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = TPE.company_id
    AND ER.DataConnectionID = TPE.data_connection_id
