SELECT
  -- system information
  ILE.stage_id AS StageID
  , ILE.Company_Id AS CompanyID
  , ILE.data_connection_id AS DataConnectionID
  -- dimensions
  , ILE.[Entry No_] AS ItemLedgerEntryCode
  , ILE.[Item No_] AS ItemCode
  , ILE.[Posting Date] AS PostingDate
  , ILE.[Entry Type] AS ItemLedgerEntryTypeCode
  , ILE.[Source No_] AS SourceCode
  , ILE.[Document No_] AS DocumentNo
  , ILE.[Document Type] AS DocumentTypeCode
  , ILE.[Location Code] AS LocationCode
  , ILE.[Variant Code] AS ItemVariantCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , ILE.Quantity AS Quantity
  , CASE 
    WHEN ILE.[Entry Type] IN (
        0
        , 2
        , 6
        )
      THEN ILE.Quantity
    ELSE NULL
    END AS PositiveQuantity
  , CASE 
    WHEN ILE.[Entry Type] IN (
        1
        , 3
        , 5
        )
      THEN ILE.Quantity
    ELSE NULL
    END AS NegativeQuantity
  , VE.[Invoiced Quantity] AS IssueQuantity
  , VE.[Valued Quantity] AS ReceiptQuantity
-- measures_RCY
FROM Stage_Nav.[Item Ledger Entry] AS ILE
LEFT JOIN stage_nav.[Value Entry] AS VE
  ON VE.[Entry No_] = ILE.[Entry No_]
    AND VE.Company_Id = ILE.Company_Id
    AND VE.Data_Connection_Id = ILE.Data_Connection_Id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON ILE.[Dimension Set ID] = FD.DimensionSetNo
    AND ILE.company_id = FD.CompanyID
    AND ILE.data_connection_Id = FD.DataConnectionID
