SELECT
  -- system information
  VE.stage_id AS StageID
  , VE.company_id AS CompanyID
  , VE.data_connection_id AS DataConnectionID
  , 1 AS SalesValueEntryType
  -- business key
  , VE.[Document No_] AS DocumentNo
  , SVE.Type AS SalesLineTypeCode
  , VE.[Item Ledger Entry No_] AS ItemLedgerEntryNo
  , VE.[Entry No_] AS ValueEntryNo
  -- Other fields
  , VE.[Source Type] AS SourceType
  , VE.[Source Posting Group] AS SourcePostingGroup
  -- dimensions                                                                       
  , VE.[Item No_] AS ItemCode
  , CAST(NULL AS NVARCHAR(20)) AS GeneralLedgerAccountCode
  , CAST(NULL AS NVARCHAR(20)) AS ResourceCode
  , CAST(NULL AS NVARCHAR(20)) AS FixedAssetCode
  , VE.[Item Charge No_] AS ItemChargeCode
  , SVE.DocumentPostingDate AS DocumentPostingDate
  , VE.[Posting Date] AS PostingDate
  , SVE.SelltoCustomerNo AS SellToCustomerCode
  , SVE.BilltoCustomerNo AS BillToCustomerCode
  , SVE.DocumentType AS DocumentTypeCode
  , VE.[Location Code] AS LocationCode
  , VE.[Variant Code] AS ItemVariantCode
  , VE.[Source Code] AS SourceCode
  , VE.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , VE.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , VE.[Return Reason Code] AS ReturnReasonCode
  , VE.[Reason Code] AS ReasonCode
  , VE.[Salespers__Purch_ Code] AS SalesPersonPurchaserCode
  , SVE.CurrencyCode AS CurrencyCode
  , VE.[Inventory Posting Group] AS InventoryPostingGroupCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , CASE 
    WHEN SVE.Type <> 1
      THEN - VE.[Invoiced Quantity]
    ELSE 0
    END AS InvoicedQuantity -- exclude GL lines
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Sales Amount (Actual)]
    ELSE 0
    END AS SalesAmount_LCY
  , VE.[Sales Amount (Actual)] + VE.[Sales Amount (Expected)] AS SalesAmountExpected_LCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Discount Amount]
    ELSE 0
    END AS DiscountAmount_LCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Amount (Actual)]
    ELSE 0
    END AS CostAmount_LCY
  , VE.[Cost Amount (Actual)] + VE.[Cost Amount (Expected)] AS CostAmountExpected_LCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Posted to G_L]
    ELSE 0
    END AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Sales Amount (Actual)] * ER.CrossRate
    ELSE 0
    END AS SalesAmount_RCY
  , (VE.[Sales Amount (Actual)] + VE.[Sales Amount (Expected)]) * ER.CrossRate AS SalesAmountExpected_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Discount Amount] * ER.CrossRate
    ELSE 0
    END AS DiscountAmount_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Amount (Actual)] * ER.CrossRate
    ELSE 0
    END AS CostAmount_RCY
  , (VE.[Cost Amount (Actual)] + VE.[Cost Amount (Expected)]) * ER.CrossRate AS CostAmountExpected_RCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Posted to G_L] * ER.CrossRate
    ELSE 0
    END AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN VE.[Sales Amount (Actual)] * SVE.CurrencyFactor
    ELSE 0
    END AS SalesAmount_PCY
  , (VE.[Sales Amount (Actual)] + VE.[Sales Amount (Expected)]) * SVE.CurrencyFactor AS SalesAmountExpected_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - (VE.[Discount Amount] * SVE.CurrencyFactor)
    ELSE 0
    END AS DiscountAmount_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Amount (Actual)] * SVE.CurrencyFactor
    ELSE 0
    END AS CostAmount_PCY
  , (VE.[Cost Amount (Actual)] + VE.[Cost Amount (Expected)]) * SVE.CurrencyFactor AS CostAmountExpected_PCY
  , CASE 
    WHEN VE.[Expected Cost] = 0
      THEN - VE.[Cost Posted to G_L] * SVE.CurrencyFactor
    ELSE 0
    END AS CostAmountPostedToGL_PCY
FROM stage_nav.[Value Entry] AS VE
INNER JOIN help.SalesValueEntry AS SVE
  ON VE.[Entry No_] = SVE.EntryNo
    AND VE.company_id = SVE.CompanyID
    AND VE.data_connection_id = SVE.DataConnectionID
LEFT JOIN help.ExchangeRates AS ER
  ON VE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyID = VE.company_id
    AND ER.DataConnectionID = VE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON VE.[Dimension Set ID] = FD.dimensionsetno
    AND VE.company_id = FD.CompanyID
    AND VE.data_connection_id = FD.DataConnectionID
WHERE VE.[Item Ledger Entry Type] = 1 --Sales
