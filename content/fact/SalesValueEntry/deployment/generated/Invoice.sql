SELECT
  -- system information
  SIL.stage_id AS StageID
  , SIL.company_id AS CompanyID
  , SIL.data_connection_id AS DataConnectionID
  , 2 AS SalesValueEntryType
  -- business key
  , SIL.[Document No_] AS DocumentNo
  , SIL.Type AS SalesLineTypeCode
  , 0 AS ItemLedgerEntryNo
  , 0 AS ValueEntryNo
  -- Other fields
  , 0 AS SourceType
  , CAST(NULL AS NVARCHAR(20)) AS SourcePostingGroup
  -- dimensions                                                          
  , CAST(NULL AS NVARCHAR(20)) AS ItemCode --comes from Value Entry table
  , CASE 
    WHEN SIL.Type = 1
      THEN SIL.No_
    ELSE ''
    END AS GeneralLedgerAccountCode
  , CASE 
    WHEN SIL.Type = 3
      THEN SIL.No_
    ELSE ''
    END AS ResourceCode
  , CASE 
    WHEN SIL.Type = 4
      THEN SIL.No_
    ELSE ''
    END AS FixedAssetCode
  , CASE 
    WHEN SIL.Type = 5
      THEN SIL.No_
    ELSE ''
    END AS ItemChargeCode
  , SIL.[Posting Date] AS DocumentPostingDate
  , SIH.[Posting Date] AS PostingDate
  , SIH.[Sell-to Customer No_] AS SellToCustomerCode
  , SIH.[Bill-to Customer No_] AS BillToCustomerCode
  , 2 AS DocumentTypeCode -- Invoice
  , SIL.[Location Code] AS LocationCode
  , CAST(NULL AS NVARCHAR(20)) AS ItemVariantCode
  , SIH.[Source Code] AS SourceCode
  , SIL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , SIL.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , CAST(NULL AS NVARCHAR(20)) AS ReturnReasonCode
  , SIH.[Reason Code] AS ReasonCode
  , SIH.[Salesperson Code] AS SalesPersonPurchaserCode
  , SIH.[Currency Code] AS CurrencyCode
  , CAST(NULL AS NVARCHAR(20)) AS InventoryPostingGroupCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures                                                            
  , CASE 
    WHEN SIL.Type <> 1
      THEN SIL.[Quantity (Base)]
    ELSE 0
    END AS InvoicedQuantity -- exclude GL lines
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN SIL.Amount / SIH.[Currency Factor]
    ELSE SIL.Amount
    END AS SalesAmount_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_LCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN SIL.[Line Discount Amount] / SIH.[Currency Factor]
    ELSE SIL.[Line Discount Amount]
    END AS DiscountAmount_LCY
  , CASE 
    WHEN SIL.Type <> 1
      THEN SIL.[Unit Cost (LCY)] * SIL.[Quantity (Base)]
    ELSE 0
    END AS CostAmount_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_LCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.Amount / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.Amount * ER.CrossRate
    END AS SalesAmount_RCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_RCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.[Line Discount Amount] / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.[Line Discount Amount] * ER.CrossRate
    END AS DiscountAmount_RCY
  , CASE 
    WHEN SIL.Type <> 1
      THEN SIL.[Unit Cost] * SIL.[Quantity (Base)] * ER.CrossRate
    ELSE 0
    END AS CostAmount_RCY -- exclude GL lines
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_RCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.Amount / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.Amount * ER.CrossRate
    END AS SalesAmount_PCY
  , CAST(NULL AS DECIMAL(38, 20)) AS SalesAmountExpected_PCY
  , CASE 
    WHEN SIH.[Currency Factor] <> 0
      THEN (SIL.[Line Discount Amount] / SIH.[Currency Factor]) * ER.CrossRate
    ELSE SIL.[Line Discount Amount] * ER.CrossRate
    END AS DiscountAmount_PCY
  , CASE 
    WHEN SIL.Type <> 1
      THEN (SIL.[Unit Cost] * SIL.[Quantity (Base)]) * ER.CrossRate
    ELSE 0
    END AS CostAmount_PCY -- exclude GL lines
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountExpected_PCY
  , CAST(NULL AS DECIMAL(38, 20)) AS CostAmountPostedToGL_PCY
FROM stage_nav.[Sales Invoice Line] AS SIL
LEFT JOIN stage_nav.[Sales Invoice Header] AS SIH
  ON SIL.[Document No_] = SIH.No_
    AND SIL.company_id = SIH.company_id
    AND SIL.data_connection_id = SIH.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON SIL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = SIL.company_id
    AND ER.DataConnectionID = SIL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON SIL.[Dimension Set ID] = FD.dimensionsetno
    AND SIL.company_id = FD.CompanyID
    AND SIL.data_connection_id = FD.DataConnectionID
WHERE SIL.Type NOT IN (
    2
    , 6
    ) -- Item, Itemcharge
