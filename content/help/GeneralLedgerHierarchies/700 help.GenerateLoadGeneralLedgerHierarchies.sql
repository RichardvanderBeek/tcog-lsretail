/****************************************************************************************************
  Functionality: Generates a procedure that loads the generalledgerhierarchies, 
                this is included in the processflow, to regerenate the procedure for every process
  Created by:    JvL	Date:	2019/02/26
  Date 		Changed by 	Ticket/Change 	Description
*****************************************************************************************************/
EXEC dbo.drop_object @object = N'help.GenerateLoadGeneralLedgerHierarchies', @type = N'P' ;
GO
CREATE PROCEDURE help.GenerateLoadGeneralLedgerHierarchies
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @debug                  BIT      = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN

  DECLARE @select_columns     NVARCHAR(MAX) = N''
        , @insert_columns     NVARCHAR(MAX) = N''
        , @outer_apply        NVARCHAR(MAX) = N''
        , @coalesce_leaf_code NVARCHAR(MAX)
        , @coalesce_leaf_name NVARCHAR(MAX)
        , @coalesce_code      NVARCHAR(MAX)
        , @coalesce_name      NVARCHAR(MAX)
        , @create_procedure   NVARCHAR(MAX) ;
  DECLARE @table_object_identifier NVARCHAR(255) = N'help.GeneralLedgerHierarchies' ;
  DECLARE @procedure_object_identifier NVARCHAR(255) = N'help.LoadGeneralLedgerHierarchies' ;
  DECLARE @general_ledger_hierarchies_max_level INT ;

  /****************************************************************************************************
      Functionality:  Determine to generate columns and outerapply's for the different parts of the query
  *****************************************************************************************************/

  SELECT @general_ledger_hierarchies_max_level = help.GetGeneralLegderHierachiesMaxLevel() ;

  -- to determine the level value a coalesce is created on lvl0 to lvln
  SELECT @coalesce_code = N'  , COALESCE( ' ;
  SELECT @coalesce_name = N'  , COALESCE( ' ;

  WITH hierarchy_levels (lvl) AS (
    SELECT @general_ledger_hierarchies_max_level -- Seed Row
    UNION ALL
    SELECT hierarchy_levels.lvl - 1 -- Recursion
      FROM hierarchy_levels
     WHERE hierarchy_levels.lvl > 0
  )
  SELECT @coalesce_code += N'NULLIF([l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_code],''''), '
       , @coalesce_name += N'NULLIF([l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_name],''''), '
    FROM hierarchy_levels AS cte
   ORDER BY cte.lvl ASC ; -- sorted ascending

  -- create select, insert and outer apply statements
  WITH hierarchy_levels (lvl) AS (
    SELECT @general_ledger_hierarchies_max_level -- Seed Row
    UNION ALL
    SELECT hierarchy_levels.lvl - 1 -- Recursion
      FROM hierarchy_levels
     WHERE hierarchy_levels.lvl > 0
  )
  SELECT @select_columns += CHAR(10) + N'  ' + @coalesce_code + N' [l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_code] ) AS LevelCode'
                            + CAST(cte.lvl AS NVARCHAR(10)) + CHAR(10) + N'  ' + @coalesce_name + N' [l' + CAST(cte.lvl AS NVARCHAR(10))
                            + N'].[level_name] ) AS LevelName' + CAST(cte.lvl AS NVARCHAR(10))
       , @insert_columns += CHAR(10) + N'  ' + N', LevelCode' + CAST(cte.lvl AS NVARCHAR(10)) + CHAR(10) + N'  ' + N', LevelName'
                            + CAST(cte.lvl AS NVARCHAR(10))
       , @outer_apply += CASE
                           WHEN cte.lvl > 0 THEN
                             CHAR(10) + ' OUTER APPLY' + CHAR(10) + '       OPENJSON(l' + CAST((cte.lvl - 1) AS NVARCHAR(10)) + '.children)' + CHAR(10)
                             + '         WITH ( level_code NVARCHAR(40) ''$.code''' + CHAR(10) + '             , level_name NVARCHAR(40) ''$.name''' + CHAR(10)
                             + '             , children NVARCHAR(MAX) ''$.children'' AS JSON) AS l' + CAST(cte.lvl AS NVARCHAR(10))
                           ELSE '' --outer apply is not required for the level 0
                         END

       -- use 'replace' after every iteration, to remove the current level
       -- the result can be use in the next iteration
       , @coalesce_code = REPLACE(@coalesce_code, 'NULLIF([l' + CAST(cte.lvl AS NVARCHAR(10)) + '].[level_code],''''), ', '')
       , @coalesce_name = REPLACE(@coalesce_name, 'NULLIF([l' + CAST(cte.lvl AS NVARCHAR(10)) + '].[level_name],''''), ', '')
    FROM hierarchy_levels AS cte
   ORDER BY cte.lvl ASC ;

  -- to determine the leaf value a coalesce is created on lvln to lvl0
  SELECT @coalesce_leaf_code = CHAR(10) + N'  , COALESCE(' ;
  SELECT @coalesce_leaf_name = CHAR(10) + N'  , COALESCE(' ;

  WITH hierarchy_levels (lvl) AS (
    SELECT @general_ledger_hierarchies_max_level -- Seed Row
    UNION ALL
    SELECT hierarchy_levels.lvl - 1 -- Recursion
      FROM hierarchy_levels
     WHERE hierarchy_levels.lvl > 0
  )
  SELECT @coalesce_leaf_code += N'[l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_code], '
       , @coalesce_leaf_name += N'[l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_name], '
    FROM hierarchy_levels AS cte
   ORDER BY cte.lvl DESC ;

  SELECT @coalesce_leaf_code = LEFT(@coalesce_leaf_code, LEN(@coalesce_leaf_code) - 1) + N') AS LeafCode' ;
  SELECT @coalesce_leaf_name = LEFT(@coalesce_leaf_name, LEN(@coalesce_leaf_name) - 1) + N') AS LeafName' ;

  /****************************************************************************************************
      Functionality:  Create the Procedure SQL and Deploy the Procedure  
  *****************************************************************************************************/

  SELECT @create_procedure
    = N'CREATE PROCEDURE ' + @procedure_object_identifier + CHAR(10)
      + N'@component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @debug                  BIT     = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS'                          + CHAR(10)
      + N'
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ; 

  DECLARE @json NVARCHAR(MAX) ;
  SELECT @json = help.GeneralLedgerHierarchiesAsJson() ;
  IF @debug = 1 
  BEGIN
    EXEC dbo.long_print @json
  END  ;  ' ;


  SELECT @create_procedure += CHAR(10) + N'
  INSERT INTO '               + @table_object_identifier + N' (
    StageId
  , ComponentExecutionId
  , CompanyId
  , DataConnectionId'         + @insert_columns + CHAR(10) + N', Code' + CHAR(10) + N', Name)' ;
  SELECT @create_procedure +=+CHAR(10) + N'SELECT 
    l0.stage_id
  , @component_execution_id
  , l0.company_id 
  , l0.data_connection_id '  + @select_columns + @coalesce_leaf_code + @coalesce_leaf_name ;
  SELECT @create_procedure += CHAR(10)
                              + N'FROM
       OPENJSON(@json)
 OUTER APPLY
       OPENJSON([value])
         WITH (stage_id INT ''$.stage_id''
             , data_connection_id INT ''$.data_connection_id''
             , company_id INT ''$.company_id''
             , level_code NVARCHAR(40) ''$.code''
             , level_name NVARCHAR(40) ''$.name''
             , children NVARCHAR(MAX) ''$.children'' AS JSON) AS l0' + @outer_apply ;

  SELECT @create_procedure += CHAR(10) + N' SELECT @inserted = @@ROWCOUNT ;' ;

  SELECT @create_procedure +=+CHAR(10) + N'END ;' ;


  EXEC dbo.drop_object @object = @procedure_object_identifier, @type = N'P', @debug = @debug ;
  IF @debug = 1 BEGIN
PRINT 'GO' ;
EXEC dbo.long_print @str = @create_procedure ;
PRINT 'GO' ;
  END ;
  ELSE BEGIN

EXEC sys.sp_executesql @create_procedure ;
  END ;

END ;

GO

EXEC help.GenerateLoadGeneralLedgerHierarchies @debug = 0 ;