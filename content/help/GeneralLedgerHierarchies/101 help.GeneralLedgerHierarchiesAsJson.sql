/****************************************************************************************************
  Functionality: Returns Json containin the entire hierarchy for the GeneralLegderAccounts
    [{  
        "no"    : "01"
      , "name"  : "gl-account-01"
      , "level" : 0 
      , "data_connection_id" : 1
      , "company_id" : 2
      , "children" : [
        -- any children are defined in here
      ]
    }]

  Created by:    JvL	Date:	2019/02/26
  
  Date 		  Changed by 	Ticket/Change 	Description
2019/04/15 	JvL 			  DEV-2070 		    now supports the start of a hierarchy on indentation > 0
*****************************************************************************************************/

EXEC dbo.drop_object @object = N'help.GeneralLedgerHierarchiesAsJson', @type = N'F' ;
GO
CREATE FUNCTION help.GeneralLedgerHierarchiesAsJson
()
RETURNS NVARCHAR(MAX)
BEGIN
  DECLARE @json NVARCHAR(MAX) ;

  SELECT @json
    = STUFF(
  ( SELECT ' '
           + CASE
               WHEN LAG(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) IS NULL THEN -- first record for each company
                 '[' + REPLICATE(help.GetGLHierarchyJsonObjectPart(t.stage_id, NULL, NULL, NULL, t.data_connection_id, t.company_id), t.Indentation)
                 + help.GetGLHierarchyJsonObjectPart(t.stage_id, t.No_, t.Name, t.Indentation, t.data_connection_id, t.company_id)
               WHEN LAG(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) < t.Indentation THEN -- one or more level deeper
                 REPLICATE(
                   help.GetGLHierarchyJsonObjectPart(t.stage_id, NULL, NULL, NULL, t.data_connection_id, t.company_id)
                 , t.Indentation - LAG(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) - 1)
                 + help.GetGLHierarchyJsonObjectPart(t.stage_id, t.No_, t.Name, t.Indentation, t.data_connection_id, t.company_id)
               WHEN LAG(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) = t.Indentation THEN -- same level
                 ']}, ' + help.GetGLHierarchyJsonObjectPart(t.stage_id, t.No_, t.Name, t.Indentation, t.data_connection_id, t.company_id)
               WHEN LAG(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) > t.Indentation THEN -- one or more level higher
                 REPLICATE(']}', LAG(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) - t.Indentation + 1) + ', '
                 + help.GetGLHierarchyJsonObjectPart(t.stage_id, t.No_, t.Name, t.Indentation, t.data_connection_id, t.company_id)
               ELSE ''
             END
           + CASE
               WHEN LEAD(t.Indentation) OVER (PARTITION BY t.company_id, t.data_connection_id ORDER BY t.No_) IS NULL THEN
                 REPLICATE(']}', t.Indentation + 1) + '],'
               ELSE ''
             END -- close the json 
      FROM stage_nav.[G_L Account]
AS                         t
     WHERE t.[Account Type] <> 4 -- exclude totals as part of the hierarchy
       AND t.execution_flag <> 'D'
     ORDER BY t.company_id
            , t.data_connection_id
            , t.No_
    FOR XML PATH, TYPE).value(N'.[1]', N'NVARCHAR(MAX)')
, 1
, 1
, N'') ;

  SET @json = N'[' + LEFT(@json, LEN(@json) - 1) + N']' ;

  RETURN @json ;
END ;
