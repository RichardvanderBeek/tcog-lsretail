/*************************************************************************************
Functionality:   This procedure can add a new custom enumeration to the help.Enumerations
Created by:      Jeroen van Lier    Date:              16-01-2019

Date            Changed By        Ticket/Change      Description
2020-04-30      Ruben Oudshoorn   2484               Add source data connections to joins

**************************************************************************************/
EXEC dbo.drop_object @object = N'help.AddCustomEnumeration', @type = N'P' ;
GO
CREATE PROCEDURE help.AddCustomEnumeration
  @component_execution_id INT
, @table_name             NVARCHAR(128)
, @column_name            NVARCHAR(128)
, @option_id              INT
, @option_value           NVARCHAR(255)
AS
BEGIN
  UPDATE      help.Enumerations
     SET
    -- System information
              ComponentExecutionId = @component_execution_id
            -- Attributes
            , OptionValue = @option_value
            , IsCustom = 1
    FROM      meta.data_connections AS dc
   CROSS JOIN help.Enumerations     AS enums
   WHERE      enums.TableName = @table_name
     AND      enums.ColumnName     = @column_name
     AND      enums.OptionNo       = @option_id
     AND      dc.is_source         = 1 
     AND      dc.data_connection_id = enums.DataConnectionId;

  INSERT INTO help.Enumerations (
    ComponentExecutionId
  , DataConnectionId
  , TableName
  , ColumnName
  , OptionNo
  , OptionValue
  , IsCustom
  )
  SELECT @component_execution_id
       , FDV.data_connection_id
       , @table_name
       , @column_name
       , @option_id
       , @option_value
       , 1
    FROM meta.data_connections AS FDV
   WHERE FDV.is_source         = 1
        AND NOT EXISTS ( SELECT *
                        FROM help.Enumerations AS enums
                       WHERE TableName  = @table_name
                         AND ColumnName = @column_name
                         AND OptionNo   = @option_id
                         AND FDV.data_connection_id = enums.DataConnectionId) ;
END ;
GO