/*************************************************************************************
Functionality:   This SQL script Loads the view "dim.FinancialDimensionsView" into the table "dim.FinancialDimensions"
Created by:      Jeroen van Lier    Date:              16-01-2019

Date            Changed By        Ticket/Change      Description
201-01-2019     Henk de Geit      PETROL-2            Fixed fieldorder INSERT INTO help.FinancialDimensions
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.LoadFinancialDimensions', @type = N'P' ;
GO
CREATE PROCEDURE help.LoadFinancialDimensions
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;
  UPDATE      help.FinancialDimensions
     SET
    -- System information
              ComponentExecutionId = @component_execution_id
            -- Attributes
            , DimensionValueCode = FDV.DimensionValueCode
            , DimensionValueName = FDV.DimensionValueName
            , DimensionValueCodeName = FDV.DimensionValueCodeName
    FROM      help.FinancialDimensionsView AS FDV
   INNER JOIN help.FinancialDimensions     AS FD ON FDV.DimensionCode    = FD.DimensionCode
                                                AND FDV.DimensionSetNo   = FD.DimensionSetNo
                                                AND FDV.CompanyId        = FD.CompanyId
                                                AND FDV.DataConnectionId = FD.DataConnectionId ;
  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO help.FinancialDimensions (
    ComponentExecutionId
  , CompanyId
  , DataConnectionId
  , FinancialDimensionsId
  , DimensionCode
  , DimensionGlobalSequence
  , DimensionSetNo
  , DimensionValueNo
  , DimensionValueCode
  , DimensionValueName
  , DimensionValueCodeName
  )
  SELECT @component_execution_id
       , FDV.CompanyId
       , FDV.DataConnectionId
       , FDV.FinancialDimensionsId
       , FDV.DimensionCode
       , FDV.DimensionGlobalSequence
       , FDV.DimensionSetNo
       , FDV.DimensionValueNo
       , FDV.DimensionValueCode
       , FDV.DimensionValueName
       , FDV.DimensionValueCodeName
    FROM help.FinancialDimensionsView AS FDV
   WHERE NOT EXISTS ( SELECT *
                        FROM help.FinancialDimensions AS FD
                       WHERE FDV.DimensionCode    = FD.DimensionCode
                         AND FDV.DimensionSetNo   = FD.DimensionSetNo
                         AND FDV.CompanyId        = FD.CompanyId
                         AND FDV.DataConnectionId = FD.DataConnectionId) ;
  SELECT @inserted = @@ROWCOUNT ;
END ;
GO