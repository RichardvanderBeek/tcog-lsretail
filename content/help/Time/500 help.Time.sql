EXEC dbo.drop_object @object = N'help.Time', @type = N'T' ;
GO

CREATE TABLE help.Time (
    [TimeID]   INT        IDENTITY(1, 1) NOT NULL PRIMARY KEY NONCLUSTERED
	, ExecutionTimestamp     ROWVERSION
, ComponentExecutionId   INT
	, Time		int NOT NULL
	, Hours		int NOT NULL
	, Quarters	int NOT NULL
	, Minutes	int NOT NULL
	, Seconds	int NOT NULL
	, TimeStrMinutes	nvarchar(10) NOT NULL
	, TimeStrSecond nvarchar(15) NOT NULL
	, HourIntervalStr nvarchar(20) NOT NULL
	, QuarterIntervalStr nvarchar(20) NOT NULL

) ON [PRIMARY] ;
