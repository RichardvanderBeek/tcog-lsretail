-- ############################################################################ --
-- #####                         Procedure: help.LoadTime				  ##### --
-- ############################################################################ --
EXEC dbo.drop_object @object = N'help.LoadTime', @type = N'P' ;  
GO
CREATE PROCEDURE help.LoadTime
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
 AS
BEGIN	
	declare @time as int;
	declare @hours as int;
	declare @quarters as int;
	declare @minutes as int;
	declare @seconds as int;
	declare @hoursStr as nvarchar(2);
	declare @minutesStr as nvarchar(2);
	declare @secondsStr as nvarchar(2);
	declare @hourIntervalStr as nvarchar(20);
	declare @quarterIntervalStr as nvarchar(20);
	
	set @time = 0;
	
	if(not exists(select * from help.Time where time = 86399))
	begin
		while(@time < 86400) -- 24h * 60m * 60s
		begin
			
			set @hours		= FLOOR(@time/3600);
			set @quarters	= FLOOR((@time-@hours*3600)/(3600/4));
			set @minutes	= FLOOR((@time-@hours*3600)/(3600/60));
			set @seconds	= FLOOR((@time-@hours*3600-@minutes*60));
			
			if(@hours = 23)
			begin
				set @hourIntervalStr = '23 - 24 uur';
			end
			else
			begin
				set @hourIntervalStr = cast(@hours as nvarchar(2)) + ' - ' + cast(@hours +1 as nvarchar(2)) + ' uur';
			end

			set @quarterIntervalStr = 
			case @quarters 
				when 0 then '0 - 15 min'
				when 1 then '15 - 30 min'
				when 2 then '30 - 45 min'
				when 3 then '45 - 60 min'
			end
			

			if(@hours < 10)
			begin
				set @hoursStr = '0' + cast(@hours as nvarchar(1));
			end
			else
			begin
				set @hoursStr = cast(@hours as nvarchar(2));
			end
			
			 
			if(@minutes < 10)
			begin
				set @minutesStr = '0' + cast(@minutes as nvarchar(1));
			end
			else
			begin
				set @minutesStr = cast(@minutes as nvarchar(2));
			end
			
			if(@seconds < 10)
			begin
				set @secondsStr = '0' + cast(@seconds as nvarchar(1));
			end
			else
			begin
				set @secondsStr = cast(@seconds as nvarchar(2));
			end
			
			insert into help.time
			(
				 ComponentExecutionId
				, Time
				,Hours
				,Quarters
				,Minutes
				,Seconds
				,TimeStrMinutes
				,TimeStrSecond
				,HourIntervalStr
				,QuarterIntervalStr
			)
			select
				 COALESCE(@component_execution_id, -1)
				,@time
				,@hours
				,@quarters
				,@minutes
				,@seconds
				,@hoursStr + ':' + @minutesStr
				,@hoursStr + ':' + @minutesStr + ':'+ @secondsStr
				,@hourIntervalStr
				,@quarterIntervalStr	
			set @time = @time + 1
		end
	end
  SET @inserted = @time
END
GO

