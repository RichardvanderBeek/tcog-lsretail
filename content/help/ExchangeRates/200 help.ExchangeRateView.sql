EXEC dbo.drop_object @object = N'help.ExchangeRatesView', @type = N'V' ;
GO
CREATE VIEW help.ExchangeRatesView
AS
  SELECT cer.data_connection_id                                          AS DataConnectionId
       , cer.company_id                                                  AS CompanyId
       , cer.[Currency Code]                                             AS ToCurrencyCode
       , cer.[Starting Date]                                             AS ValidFrom
       , DATEADD(d
               , -1
               , COALESCE(LEAD(cer.[Starting Date]) OVER (PARTITION BY cer.data_connection_id
                                                                     , cer.company_id
                                                                     , cer.[Currency Code]
                                                              ORDER BY cer.[Starting Date])
                        , '9999-12-31'))                                 AS ValidTo
       , cer.[Exchange Rate Amount] / cer.[Relational Exch_ Rate Amount] AS CrossRate
    FROM stage_nav.[Currency Exchange Rate] AS cer
   WHERE cer.[Relational Exch_ Rate Amount] <> 0
     AND cer.[Relational Exch_ Rate Amount] IS NOT NULL
     AND EXISTS (SELECT * FROM meta.current_instance WHERE cer.[Currency Code] = reporting_currency_code)
     AND cer.execution_flag                 <> 'D' ;
