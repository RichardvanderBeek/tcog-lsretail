/*************************************************************************************
Functionality:   This procedure can add a new custom enumeration to the help.GLAccountHierarchy
Created by:      Jeroen van Lier    Date:              16-01-2019

Date            Changed By        Ticket/Change      Description
2020-04-30      Ruben Oudshoorn   2484               Add source data connections to joins

**************************************************************************************/
EXEC dbo.drop_object @object = N'help.AddGLAccountHierarchy', @type = N'P' ;
GO
CREATE PROCEDURE help.AddGLAccountHierarchy
  @component_execution_id INT
, @AccountType            NVARCHAR(255)
, @Level1Account          NVARCHAR(255)
, @Level1Desc             NVARCHAR(255)
, @Level2Account          NVARCHAR(255)
, @Level2Desc             NVARCHAR(255)
, @Level3Account          NVARCHAR(255)
, @Level3Desc             NVARCHAR(255)
, @Level4Account          NVARCHAR(255)
, @Level4Desc             NVARCHAR(255)
, @Level5Account          NVARCHAR(255)
, @Level5Desc             NVARCHAR(255)
, @Level6Account          NVARCHAR(255)
, @Level6Desc             NVARCHAR(255)
, @Level7Account          NVARCHAR(255)
, @Level7Desc             NVARCHAR(255)
, @Level8Account          NVARCHAR(255)
, @Level8Desc             NVARCHAR(255)
, @GLAccount              NVARCHAR(255)
, @GLAccountDesc          NVARCHAR(255)
AS
BEGIN
  UPDATE      help.GLAccountHierarchy
     SET
    -- System information
              ComponentExecutionId = @component_execution_id
            -- Attributes
            , AccountType = @AccountType
            , Level1Account = @Level1Account
            , Level1Desc = @Level1Desc
            , Level2Account = @Level2Account
            , Level2Desc = @Level2Desc
            , Level3Account = @Level3Account
            , Level3Desc = @Level3Desc
            , Level4Account = @Level4Account
            , Level4Desc = @Level4Desc
            , Level5Account = @Level5Account
            , Level5Desc = @Level5Desc
            , Level6Account = @Level6Account
            , Level6Desc = @Level6Desc
            , Level7Account = @Level7Account
            , Level7Desc = @Level7Desc
            , Level8Account = @Level8Account
            , Level8Desc = @Level8Desc
            , GLAccountDesc = @GLAccountDesc
    FROM      meta.data_connections   AS dc
   CROSS JOIN help.GLAccountHierarchy AS gla
   WHERE      gla.GLAccount    = @GLAccount
     AND      dc.is_source          = 1
     AND      dc.data_connection_id = gla.DataConnectionId ;

  INSERT INTO help.GLAccountHierarchy (
    ComponentExecutionId
  , DataConnectionId
  , AccountType
  , Level1Account
  , Level1Desc
  , Level2Account
  , Level2Desc
  , Level3Account
  , Level3Desc
  , Level4Account
  , Level4Desc
  , Level5Account
  , Level5Desc
  , Level6Account
  , Level6Desc
  , Level7Account
  , Level7Desc
  , Level8Account
  , Level8Desc
  , GLAccount
  , GLAccountDesc
  )
  SELECT @component_execution_id
       , dc.data_connection_id
       , @AccountType
       , @Level1Account
       , @Level1Desc
       , @Level2Account
       , @Level2Desc
       , @Level3Account
       , @Level3Desc
       , @Level4Account
       , @Level4Desc
       , @Level5Account
       , @Level5Desc
       , @Level6Account
       , @Level6Desc
       , @Level7Account
       , @Level7Desc
       , @Level8Account
       , @Level8Desc
       , @GLAccount
       , @GLAccountDesc
    FROM meta.data_connections AS dc
   WHERE dc.is_source = 1
     AND NOT EXISTS ( SELECT *
                        FROM help.GLAccountHierarchy AS gla
                       WHERE gla.GLAccount         = @GLAccount
                         AND dc.data_connection_id = gla.DataConnectionId) ;
END ;
GO