EXEC dbo.drop_object @object = N'help.AddGLAccountHierarchyCSV', @type = N'P' ;
GO
CREATE PROCEDURE help.AddGLAccountHierarchyCSV
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  DECLARE @location      NVARCHAR(511)
        , @csv_file_name NVARCHAR(100)
        , @fmt_file_name NVARCHAR(100) ;
  DECLARE @command NVARCHAR(1000) ;
  DECLARE @count INT = 0 ;

  --Set user location e.g. C:\Users\name\Document\GLAccountHierarchy\
  SET @location = N'C:\Users\name\Document\GLAccountHierarchy\' ; --'

  SET @csv_file_name = N'GLAccountHierarchy.csv' ;
  SET @fmt_file_name = N'GLAccountHierarchy.fmt' ;

  EXEC dbo.drop_object @object = N'help.GLAccountHierarchyCSV', @type = N'T' ;

  SET @command = N'SELECT * 
 INTO help.GLAccountHierarchyCSV
 FROM
OPENROWSET(
   BULK ''' + @location + @csv_file_name + N''',
   FIRSTROW=2,
    FORMATFILE = ''' + @location + @fmt_file_name + N''') AS DATA;' ;
  BEGIN TRY
    EXEC sp_executesql @command ;
    SELECT @count = COUNT(*) FROM help.GLAccountHierarchyCSV ;
  END TRY
  BEGIN CATCH
    RETURN 0
  END CATCH ;

  IF @count > 0
  BEGIN
    DELETE FROM help.GLAccountHierarchy ;
    SET @deleted = @@ROWCOUNT ;

    INSERT INTO help.GLAccountHierarchy (
      ComponentExecutionId
    , DataConnectionId
    , AccountType
    , Level1Account
    , Level1Desc
    , Level2Account
    , Level2Desc
    , Level3Account
    , Level3Desc
    , Level4Account
    , Level4Desc
    , Level5Account
    , Level5Desc
    , Level6Account
    , Level6Desc
    , Level7Account
    , Level7Desc
    , Level8Account
    , Level8Desc
    , GLAccount
    , GLAccountDesc
    )
    SELECT      @component_execution_id
              , dc.data_connection_id
              , COALESCE(glc.AccountType, '')
              , COALESCE(glc.Level1Account, '')
              , COALESCE(glc.Level1Desc, '')
              , COALESCE(glc.Level2Account, '')
              , COALESCE(glc.Level2Desc, '')
              , COALESCE(glc.Level3Account, '')
              , COALESCE(glc.Level3Desc, '')
              , COALESCE(glc.Level4Account, '')
              , COALESCE(glc.Level4Desc, '')
              , COALESCE(glc.Level5Account, '')
              , COALESCE(glc.Level5Desc, '')
              , COALESCE(glc.Level6Account, '')
              , COALESCE(glc.Level6Desc, '')
              , COALESCE(glc.Level7Account, '')
              , COALESCE(glc.Level7Desc, '')
              , COALESCE(glc.Level8Account, '')
              , COALESCE(glc.Level8Desc, '')
              , COALESCE(glc.GLAccount, '')
              , COALESCE(glc.GLAccountDesc, '')
      FROM      help.GLAccountHierarchyCSV glc
     CROSS JOIN meta.data_connections      AS dc
     WHERE      dc.is_source = 1 ;
  END ;

  SET @inserted = @@ROWCOUNT ;

  EXEC dbo.drop_object @object = N'help.GLAccountHierarchyCSV', @type = N'T' ;
  RETURN @count ;

END ;
