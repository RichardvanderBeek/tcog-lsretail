/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseItemLedgerEntryView
AS
  SELECT
    -- system information
         ILE.stage_id           AS StageID
       , ILE.company_id         AS CompanyID
       , ILE.data_connection_id AS DataConnectionID
                                                -- business key
       , ILE.[Entry No_]        AS EntryNo
       , 0                      AS DocumentType --Empty Document Type
       , ILE.[Document No_]     AS DocumentNo
       , ILE.[Posting Date]     AS PostingDate
       , ILE.[Posting Date]     AS DocumentPostingDate
       , ''                     AS BuyFromVendorNo
       , ''                     AS PayToVendorNo
    FROM stage_nav.[Item Ledger Entry] AS ILE
   WHERE ILE.[Entry Type]    = 0
     AND ILE.[Document Type] = 0
     AND ILE.execution_flag  <> 'D' ;
GO
