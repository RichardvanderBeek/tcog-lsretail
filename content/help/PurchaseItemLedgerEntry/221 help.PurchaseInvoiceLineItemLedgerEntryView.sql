/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseInvoiceLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseInvoiceLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseInvoiceLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    PIL.stage_id                                     AS StageID
                  , PIL.company_id                                   AS CompanyID
                  , PIL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                  AS EntryNo
                  , ILE.[Document Type]                              AS DocumentType
                  , ILE.[Document No_]                               AS DocumentNo
                  , ILE.[Posting Date]                               AS PostingDate
                  , COALESCE(PIH.[Posting Date], PIL.[Posting Date]) AS DocumentPostingDate
                  , PIL.[Buy-from Vendor No_]                        AS BuyFromVendorNo
                  , PIL.[Pay-to Vendor No_]                          AS PayToVendorNo
    FROM            stage_nav.[Purch_ Inv_ Line]   AS PIL
    LEFT OUTER JOIN stage_nav.[Purch_ Inv_ Header] AS PIH ON PIL.[Document No_]     = PIH.[No_]
                                                         AND PIL.company_id         = PIH.company_id
                                                         AND PIL.data_connection_id = PIH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]  AS ILE ON ILE.[Document Type]    = 6 --Invoice
                                                         AND PIL.[Document No_]     = ILE.[Document No_]
                                                         AND PIL.[Line No_]         = ILE.[Document Line No_]
                                                         AND ILE.[Entry Type]       = 0 --Purchase  
                                                         AND PIL.company_id         = ILE.company_id
                                                         AND PIL.data_connection_id = ILE.data_connection_id
   WHERE            PIL.execution_flag <> 'D'
     AND            PIH.execution_flag            <> 'D'
     AND            ILE.execution_flag            <> 'D' ;
GO
