/***********************************************************************************
Functionality:  This SQL script creates the help.SalesValueEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesValueEntryView', @type = N'V' ;
GO
CREATE VIEW help.SalesValueEntryView
AS
  SELECT
    -- system information
         VE.stage_id           AS StageID
       , VE.company_id         AS CompanyID
       , VE.data_connection_id AS DataConnectionID
       , VE.execution_timestamp AS execution_timestamp
                                               -- business key
       , VE.[Entry No_]        AS EntryNo
       , 0                     AS DocumentType --Empty Document Type
       , VE.[Document No_]     AS DocumentNo
       , VE.[Posting Date]     AS PostingDate
       , VE.[Posting Date]     AS DocumentPostingDate
       , ''                    AS SelltoCustomerNo
       , ''                    AS BilltoCustomerNo
       , 1                     AS CurrencyFactor
       , ''                    AS CurrencyCode
       , VE.[Type]             AS [Type]
    FROM stage_nav.[Value Entry] AS VE
   WHERE VE.[Item Ledger Entry Type] = 1
     AND VE.[Document Type]          = 0
     AND VE.execution_flag          <> 'D' ;
GO
