/***********************************************************************************
Functionality:  This SQL script creates the help.SalesCreditMemoLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesCreditMemoLineView', @type = N'V' ;
GO
CREATE VIEW help.SalesCreditMemoLineView
AS
  SELECT
    -- system information
                    SCML.stage_id                                      AS StageID
                  , SCML.company_id                                    AS CompanyID
                  , SCML.data_connection_id                            AS DataConnectionID
                  , VE.execution_timestamp                             AS execution_timestamp
                  -- business key
                  , VE.[Entry No_]                                     AS EntryNo
                  , VE.[Document Type]                                 AS DocumentType
                  , VE.[Document No_]                                  AS DocumentNo
                  , VE.[Posting Date]                                  AS PostingDate
                  , COALESCE(SCMH.[Posting Date], SCML.[Posting Date]) AS DocumentPostingDate
                  , SCML.[Sell-to Customer No_]                        AS SelltoCustomerNo
                  , SCML.[Bill-to Customer No_]                        AS BilltoCustomerNo
                  , COALESCE(SCMH.[Currency Factor], 1)                AS CurrencyFactor
                  , SCMH.[Currency Code]                               AS CurrencyCode
                  , SCML.[Type]                                        AS [Type]
    FROM            stage_nav.[Sales Cr_Memo Line]   AS SCML
    LEFT OUTER JOIN stage_nav.[Sales Cr_Memo Header] AS SCMH ON SCML.[Document No_]       = SCMH.[No_]
                                                            AND SCML.company_id           = SCMH.company_id
                                                            AND SCML.data_connection_id   = SCMH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]          AS VE ON VE.[Document Type]          = 4 --Credit Memo
                                                          AND SCML.[Document No_]         = VE.[Document No_]
                                                          AND SCML.[Line No_]             = VE.[Document Line No_]
                                                          AND VE.[Item Ledger Entry Type] = 1 --Sales
                                                          AND SCML.company_id             = VE.company_id
                                                          AND SCML.data_connection_id     = VE.data_connection_id
   WHERE            SCML.execution_flag <> 'D'
     AND            SCMH.execution_flag            <> 'D'
     AND            VE.execution_flag              <> 'D' ;
GO
