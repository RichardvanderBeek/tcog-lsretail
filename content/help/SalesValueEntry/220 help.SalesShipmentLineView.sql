/***********************************************************************************
Functionality:  This SQL script creates the help.SalesShipmentLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesShipmentLineView', @type = N'V' ;
GO
CREATE VIEW help.SalesShipmentLineView
AS
  SELECT
    -- system information
                    SSSL.stage_id                                     AS StageID
                  , SSSL.company_id                                   AS CompanyID
                  , SSSL.data_connection_id                           AS DataConnectionID
                  , VE.execution_timestamp                            AS execution_timestamp
                  -- business key
                  , VE.[Entry No_]                                    AS EntryNo
                  , VE.[Document Type]                                AS DocumentType
                  , VE.[Document No_]                                 AS DocumentNo
                  , VE.[Posting Date]                                 AS PostingDate
                  , COALESCE(SSH.[Posting Date], SSSL.[Posting Date]) AS DocumentPostingDate
                  , SSSL.[Sell-to Customer No_]                       AS SelltoCustomerNo
                  , SSSL.[Bill-to Customer No_]                       AS BilltoCustomerNo
                  , COALESCE(SSH.[Currency Factor], 1)                AS CurrencyFactor
                  , SSH.[Currency Code]                               AS CurrencyCode
                  , SSSL.[Type]                                       AS [Type]
    FROM            stage_nav.[Sales Shipment Line]   AS SSSL
    LEFT OUTER JOIN stage_nav.[Sales Shipment Header] AS SSH ON SSSL.[Document No_]        = SSH.[No_]
                                                            AND SSSL.company_id            = SSH.company_id
                                                            AND SSSL.data_connection_id    = SSH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]           AS VE ON VE.[Document Type]          = 1 --Shipments
                                                           AND SSSL.[Document No_]         = VE.[Document No_]
                                                           AND SSSL.[Line No_]             = VE.[Document Line No_]
                                                           AND VE.[Item Ledger Entry Type] = 1 --Sales  
                                                           AND SSSL.company_id             = VE.company_id
                                                           AND SSSL.data_connection_id     = VE.data_connection_id
   WHERE            SSSL.execution_flag <> 'D'
     AND            SSH.execution_flag             <> 'D'
     AND            VE.execution_flag              <> 'D' ;
GO
