/***********************************************************************************
Functionality:  This SQL script creates the help.LoadSalesValueEntry
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.LoadSalesValueEntry', @type = N'P' ;
GO

CREATE PROCEDURE help.LoadSalesValueEntry
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;


  -- full load
  IF @load_type = 0
  BEGIN
    SELECT @deleted = COUNT(1) FROM help.SalesValueEntry ;
    INSERT INTO help.SalesValueEntry (
      --system information
      StageID
    , CompanyID
    , DataConnectionID
    , ComponentExecutionID
    , execution_timestamp
    , Execution_Flag
    , EntryNo
    , DocumentType
    , DocumentNo
    , PostingDate
    , DocumentPostingDate
    , SelltoCustomerNo
    , BilltoCustomerNo
    , CurrencyFactor
    , CurrencyCode
    , [Type]
    )
    SELECT
      --system information
           SVEUV.StageID
         , SVEUV.CompanyID
         , SVEUV.DataConnectionID
         , @component_execution_id
         , SVEUV.execution_timestamp
         , 'U'
         , SVEUV.EntryNo
         , SVEUV.DocumentType
         , SVEUV.DocumentNo
         , SVEUV.PostingDate
         , SVEUV.DocumentPostingDate
         , SVEUV.SelltoCustomerNo
         , SVEUV.BilltoCustomerNo
         , SVEUV.CurrencyFactor
         , SVEUV.CurrencyCode
         , SVEUV.[Type]
      FROM help.SalesValueEntryUnionView AS SVEUV ;
    SELECT @inserted = @@ROWCOUNT ;
  END ;
END ;