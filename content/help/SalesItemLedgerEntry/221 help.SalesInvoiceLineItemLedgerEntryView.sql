/***********************************************************************************
Functionality:  This SQL script creates the help.SalesInvoiceLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesInvoiceLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.SalesInvoiceLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    SIL.stage_id                                     AS StageID
                  , SIL.company_id                                   AS CompanyID
                  , SIL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                  AS EntryNo
                  , ILE.[Document Type]                              AS DocumentType
                  , ILE.[Document No_]                               AS DocumentNo
                  , ILE.[Posting Date]                               AS PostingDate
                  , COALESCE(SIH.[Posting Date], SIL.[Posting Date]) AS DocumentPostingDate
                  , SIL.[Sell-to Customer No_]                       AS SelltoCustomerNo
                  , SIL.[Bill-to Customer No_]                       AS BilltoCustomerNo
    FROM            stage_nav.[Sales Invoice Line]   AS SIL
    LEFT OUTER JOIN stage_nav.[Sales Invoice Header] AS SIH ON SIL.[Document No_]     = SIH.[No_]
                                                           AND SIL.company_id         = SIH.company_id
                                                           AND SIL.data_connection_id = SIH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]    AS ILE ON ILE.[Document Type]    = 2 --Invoice
                                                           AND SIL.[Document No_]     = ILE.[Document No_]
                                                           AND SIL.[Line No_]         = ILE.[Document Line No_]
                                                           AND ILE.[Entry Type]       = 1 --Sales  
                                                           AND SIL.company_id         = ILE.company_id
                                                           AND SIL.data_connection_id = ILE.data_connection_id
   WHERE            SIL.execution_flag <> 'D'
     AND            SIH.execution_flag            <> 'D'
     AND            ILE.execution_flag            <> 'D' ;
GO
