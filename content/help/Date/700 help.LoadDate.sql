EXEC dbo.drop_object @object = N'help.LoadDate' -- nvarchar(128)
                   , @type = N'P'              -- nchar(2)
                   , @debug = 0 ;              -- int
GO

CREATE PROCEDURE help.LoadDate
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
  DECLARE @StartDate            DATETIME
        , @EndDate              DATETIME
        , @TempDate             DATETIME
        , @DateID               INT
        , @Date                 DATETIME
        , @FullDescription      NVARCHAR(255)
        , @DescYMD              NVARCHAR(255)
        , @DescDMY              NVARCHAR(255)
        , @DescMDY              NVARCHAR(255)
        , @DescYYYYMMDD         NVARCHAR(255)
        , @DescDDMMYYYY         NVARCHAR(255)
        , @DescMMDDYYYY         NVARCHAR(255)
        -- Keys
        , @CYearNo              INT
        , @CQuarterNo           TINYINT
        , @CMonthNo             SMALLINT
        , @CWeekNo              SMALLINT
        , @CIsoYearNo           INT
        , @CYearMonthNo         INT
        , @CYearQuarterNo       INT
        , @CDayInMonthNo        SMALLINT
        , @CDayWeekNo           SMALLINT
        , @CDayYearNo           INT
        , @CYearWeekNo          INT
        , @CHalfYearNo          TINYINT
        -- Description
        , @CYearDesc            NVARCHAR(255)
        , @CMonthDesc           NVARCHAR(255)
        , @CQuarterDesc         NVARCHAR(255)
        , @CWeekDesc            NVARCHAR(255)
        , @CIsoYearDesc         NVARCHAR(255)
        , @CYearMonthDesc       NVARCHAR(255)
        , @CYearQuarterDesc     NVARCHAR(255)
        , @CMonthYearDesc       NVARCHAR(255)
        , @CQuarterYearDesc     NVARCHAR(255)
        , @CYearWeekDesc        NVARCHAR(255)
        , @CWeekYearDesc        NVARCHAR(255)
        , @CDayDesc             NVARCHAR(255)
        , @CDayWeekDesc         NVARCHAR(255)
        , @CHalfYearDesc        NVARCHAR(255)
        -- Start/End of period notifier
        , @CFirstDayMonthDate   DATETIME
        , @CLastDayMonthDate    DATETIME
        , @CNumberOfDaysInMonth SMALLINT
        , @CEndMonth            TINYINT
        , @CStartMonth          TINYINT
        -- Past/Future notifier
        , @Past                 INT
        -- Days since 01/01/1900
        , @DateNo               INT
        , @CMonthNoDesc         VARCHAR(10)
        , @CDayInMonthDesc      VARCHAR(50)
        -- Columns used in tabular
        , @TDatePY              DATETIME
        , @TDateNoPY            INT
        , @TYearMonthNo         INT
        , @TMonthDays           INT ;

  SET DATEFIRST 1 ;

  SET @StartDate = CAST((SELECT date_start_year FROM meta.instances) AS NVARCHAR(255)) + '0101' ;
  SET @EndDate = CAST(YEAR(DATEADD(YEAR, (SELECT date_offset_years FROM meta.instances), GETDATE())) AS NVARCHAR(255)) + '1231' ;
  SET @TempDate = @StartDate ;

  EXEC dbo.drop_object '#TempDate', 'TT' ;

  SELECT * INTO #TempDate FROM help.Date WHERE 1 = 0 ;

  WHILE @TempDate <= @EndDate
  BEGIN
    SET @DateID = CONVERT(NVARCHAR(255), @TempDate, 112) ;
    --
    SET @FullDescription = DATENAME(DAY, @TempDate) + N'. ' + DATENAME(MONTH, @TempDate) + N', ' + DATENAME(YEAR, @TempDate) ;
    SET @DescYYYYMMDD = CONVERT(NVARCHAR(255), @TempDate, 102) ;
    SET @DescDDMMYYYY = CONVERT(NVARCHAR(255), @TempDate, 104) ;
    SET @DescMMDDYYYY = REPLACE(CONVERT(NVARCHAR(255), @TempDate, 110), '-', '.') ;
    SET @DescYMD = CAST(DATEPART(yyyy, @TempDate) AS CHAR(4)) + N'.' + CAST(DATEPART(m, @TempDate) AS VARCHAR(2)) + N'.' + CAST(DATEPART(d, @TempDate) AS VARCHAR(2)) ;
    SET @DescDMY = RIGHT(' ' + RTRIM(CAST(DATEPART(d, @TempDate) AS VARCHAR(2))), 2) + N'.' + CAST(DATEPART(m, @TempDate) AS VARCHAR(2)) + N'.' + CAST(DATEPART(yyyy, @TempDate) AS CHAR(4)) ;
    SET @DescMDY = RIGHT(' ' + RTRIM(CAST(DATEPART(m, @TempDate) AS VARCHAR(2))), 2) + N'.' + CAST(DATEPART(d, @TempDate) AS VARCHAR(2)) + N'.' + CAST(DATEPART(yyyy, @TempDate) AS CHAR(4)) ;
    -- Keys
    SET @CYearNo = DATEPART(yyyy, @TempDate) ;
    SET @CQuarterNo = DATEPART(q, @TempDate) ;
    SET @CMonthNo = DATEPART(m, @TempDate) ;
    SET @CWeekNo = DATEPART(ISO_WEEK, @TempDate) ;
    SET @CIsoYearNo = CASE
                        WHEN @CWeekNo IN ( 52, 53 )
                         AND @CMonthNo = 1 THEN -1
                        WHEN @CWeekNo = 1
                         AND @CMonthNo = 12 THEN 1
                        ELSE 0
                      END + @CYearNo ;

    SET @CYearMonthNo = @CYearNo * 100 + @CMonthNo ;
    SET @CYearQuarterNo = @CYearNo * 10 + @CQuarterNo ;
    SET @CYearWeekNo = @CIsoYearNo * 100 + @CWeekNo ;
    SET @CDayInMonthNo = DATEPART(d, @TempDate) ;
    SET @CDayWeekNo = DATEPART(WEEKDAY, @TempDate) ;
    SET @CDayYearNo = DATEPART(DAYOFYEAR, @TempDate) ;
    SET @CHalfYearNo = @CMonthNo / 7 + 1 ;
    -- Description
    SET @CYearDesc = CAST(@CYearNo AS NVARCHAR(255)) ;
    SET @CIsoYearDesc = CAST(@CIsoYearNo AS NVARCHAR(255)) ;
    SET @CMonthDesc = DATENAME(MONTH, @TempDate) ;
    SET @CQuarterDesc = N'Q' + CAST(@CQuarterNo AS NVARCHAR(255)) ;
    SET @CDayDesc = DATENAME(DAY, @TempDate) ;
    SET @CWeekDesc = RIGHT(' ' + RTRIM(CAST(@CWeekNo AS VARCHAR(2))), 2) ;
    SET @CYearMonthDesc = @CYearDesc + N' - ' + CAST(DATENAME(m, @TempDate) AS NVARCHAR(255)) ;
    SET @CYearQuarterDesc = @CYearDesc + N' - ' + @CQuarterDesc ;
    SET @CMonthYearDesc = CAST(DATENAME(m, @TempDate) AS NVARCHAR(255)) + N' - ' + @CYearDesc ;
    SET @CQuarterYearDesc = @CQuarterDesc + N' - ' + @CYearDesc ;
    SET @CYearWeekDesc = @CIsoYearDesc + N' - ' + @CWeekDesc ;
    SET @CWeekYearDesc = RIGHT(' ' + RTRIM(@CWeekDesc), 2) + N' - ' + @CIsoYearDesc ;
    SET @CHalfYearDesc = CAST(@CHalfYearNo AS NVARCHAR(255)) + N'/2' ;
    -- Start/End of period notifier
    SET @CStartMonth = CASE WHEN @CDayInMonthNo = 1 THEN 1 ELSE 0 END ;
    SET @CFirstDayMonthDate = @TempDate - DATEPART(DAY, @TempDate) + 1 ;
    SET @CLastDayMonthDate = DATEADD(m, 1, @CFirstDayMonthDate) - 1 ;
    SET @CNumberOfDaysInMonth = DATEDIFF(DAY, @CFirstDayMonthDate, @CLastDayMonthDate) + 1 ;
    SET @CEndMonth = CASE WHEN DATEPART(dd, DATEADD(DAY, 1, @TempDate)) = 1 THEN 1 ELSE 0 END ;
    -- Past/Future notifier
    SET @Past = CASE WHEN @TempDate < CONVERT(NVARCHAR(255), GETDATE(), 112) THEN 1 ELSE 0 END ;
    SET @DateNo = DATEDIFF(d, '1900-01-01', @TempDate) ;
    SET @CMonthNoDesc = RIGHT(' ' + RTRIM(DATEPART(m, @TempDate)), 2) ;
    SET @CDayWeekDesc = DATENAME(dw, @TempDate) ;
    SET @CDayInMonthDesc = RIGHT(' ' + RTRIM(DATEPART(d, @TempDate)), 2) ;
    -- Columns used in tabular
    SET @TDatePY = DATEADD(YEAR, -1, @TempDate) ;
    SET @TDateNoPY = DATEDIFF(d, '1900-01-01', @TDatePY) ;
    SET @TYearMonthNo = (@CYearNo - 1900) * 12 + @CMonthNo ;
    SET @TMonthDays = DATEPART(DAY, @CLastDayMonthDate) ;

    -- Insert temp date
    INSERT INTO #TempDate (
      DateID
    , Date
    , FullDescription
    , DescYYYYMMDD
    , DescDDMMYYYY
    , DescMMDDYYYY
    , DescYMD
    , DescDMY
    , DescMDY
    , CYearNo
    , CIsoYearNo
    , CQuarterNo
    , CMonthNo
    , CWeekNo
    , CYearMonthNo
    , CYearQuarterNo
    , CYearWeekNo
    , CDayInMonthNo
    , CDayWeekNo
    , CDayYearNo
    , CHalfYearNo
    , CYearDesc
    , CIsoYearDesc
    , CMonthDesc
    , CQuarterDesc
    , CDayDesc
    , CWeekDesc
    , CYearMonthDesc
    , CYearQuarterDesc
    , CMonthYearDesc
    , CQuarterYearDesc
    , CYearWeekDesc
    , CWeekYearDesc
    , CHalfYearDesc
    , CFirstDayMonthDate
    , CLastDayMonthDate
    , CNumberOfDaysInMonth
    , CEndMonth
    , CStartMonth
    , Past
    , DateNo
    , CMonthNoDesc
    , CDayWeekDesc
    , CDayInMonthDesc
    , TDatePY
    , TYearMonthNo
    , TMonthDays
    )
    VALUES
      (@DateID, @TempDate, @FullDescription, @DescYYYYMMDD, @DescDDMMYYYY, @DescMMDDYYYY, @DescYMD, @DescDMY, @DescMDY, @CYearNo, @CIsoYearNo, @CQuarterNo, @CMonthNo, @CWeekNo, @CYearMonthNo
     , @CYearQuarterNo, @CYearWeekNo, @CDayInMonthNo, @CDayWeekNo, @CDayYearNo, @CHalfYearNo, @CYearDesc, @CIsoYearDesc, @CMonthDesc, @CQuarterDesc, @CDayDesc, @CWeekDesc, @CYearMonthDesc
     , @CYearQuarterDesc, @CMonthYearDesc, @CQuarterYearDesc, @CYearWeekDesc, @CWeekYearDesc, @CHalfYearDesc, @CFirstDayMonthDate, @CLastDayMonthDate, @CNumberOfDaysInMonth, @CEndMonth, @CStartMonth
     , @Past, @DateNo, @CMonthNoDesc, @CDayWeekDesc, @CDayInMonthDesc, @TDatePY, @TYearMonthNo, @TMonthDays) ;
    SET @TempDate = DATEADD(DAY, 1, @TempDate) ;
  END ;

  --Update updated dates
  UPDATE      help.Date
     SET      Past = a.Past
    FROM      help.Date  b
   INNER JOIN #TempDate a ON a.DateID = b.DateID
   WHERE      a.Past <> b.Past ;

  SELECT @updated = @@ROWCOUNT ;

  -- Insert new dates
  INSERT INTO help.Date (
    DateID
  , ComponentExecutionId
  , Date
  , FullDescription
  , DescYYYYMMDD
  , DescDDMMYYYY
  , DescMMDDYYYY
  , DescYMD
  , DescDMY
  , DescMDY
  , CYearNo
  , CIsoYearNo
  , CQuarterNo
  , CMonthNo
  , CWeekNo
  , CYearMonthNo
  , CYearQuarterNo
  , CYearWeekNo
  , CDayInMonthNo
  , CDayWeekNo
  , CDayYearNo
  , CHalfYearNo
  , CYearDesc
  , CIsoYearDesc
  , CMonthDesc
  , CQuarterDesc
  , CDayDesc
  , CWeekDesc
  , CYearMonthDesc
  , CYearQuarterDesc
  , CMonthYearDesc
  , CQuarterYearDesc
  , CYearWeekDesc
  , CWeekYearDesc
  , CHalfYearDesc
  , CFirstDayMonthDate
  , CLastDayMonthDate
  , CNumberOfDaysInMonth
  , CEndMonth
  , CStartMonth
  , Past
  , DateNo
  , CMonthNoDesc
  , CDayWeekDesc
  , CDayInMonthDesc
  , TDatePY
  , TYearMonthNo
  , TMonthDays
  )
  SELECT a.DateID
       , COALESCE(@component_execution_id, -1)
       , a.Date
       , a.FullDescription
       , a.DescYYYYMMDD
       , a.DescDDMMYYYY
       , a.DescMMDDYYYY
       , a.DescYMD
       , a.DescDMY
       , a.DescMDY
       , a.CYearNo
       , a.CIsoYearNo
       , a.CQuarterNo
       , a.CMonthNo
       , a.CWeekNo
       , a.CYearMonthNo
       , a.CYearQuarterNo
       , a.CYearWeekNo
       , a.CDayInMonthNo
       , a.CDayWeekNo
       , a.CDayYearNo
       , a.CHalfYearNo
       , a.CYearDesc
       , a.CIsoYearDesc
       , a.CMonthDesc
       , a.CQuarterDesc
       , a.CDayDesc
       , a.CWeekDesc
       , a.CYearMonthDesc
       , a.CYearQuarterDesc
       , a.CMonthYearDesc
       , a.CQuarterYearDesc
       , a.CYearWeekDesc
       , a.CWeekYearDesc
       , a.CHalfYearDesc
       , a.CFirstDayMonthDate
       , a.CLastDayMonthDate
       , a.CNumberOfDaysInMonth
       , a.CEndMonth
       , a.CStartMonth
       , a.Past
       , a.DateNo
       , a.CMonthNoDesc
       , a.CDayWeekDesc
       , a.CDayInMonthDesc
       , a.TDatePY
       , a.TYearMonthNo
       , a.TMonthDays
    FROM #TempDate a
   WHERE NOT EXISTS (SELECT * FROM help.Date b WHERE a.DateID = b.DateID) ;

  SELECT @inserted = @@ROWCOUNT ;

  EXEC dbo.drop_object '#TempDate', 'TT' ;

GO
