SELECT
  -- System Information
  PS.stage_id AS StageID
  , PS.company_id AS CompanyID
  , PS.data_connection_id AS DataConnectionID
  -- Business Key
  , PS.[No_] AS POSStatementCode
  -- Attributes
  , COALESCE(NULLIF(PS.[Store No_], ''), 'N/A') AS StoreCode
  , COALESCE(NULLIF(PS.[Store No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(STR.NAME, ''), 'N/A') AS StoreCodeName
  , COALESCE(NULLIF(e_M.[OptionValue], ''), 'N/A') AS Method
  , COALESCE(NULLIF(e_CM.[OptionValue], ''), 'N/A') AS ClosingMethod
  , COALESCE(NULLIF(PS.[Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS StatementDate
  , COALESCE(NULLIF(PS.[Posting Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS StatementPostingDate
  , COALESCE(NULLIF(CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, PS.[Posted Time]), 0)), 108), ''), 'N/A') AS StatementPostedTime
  , COALESCE(NULLIF(PS.[Calculated Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS StatementCalculatedDate
  , COALESCE(NULLIF(CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, PS.[Calculated Time]), 0)), 108), ''), 'N/A') AS StatementCalculatedTime
  , COALESCE(NULLIF(PS.[Trans_ Starting Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS StatementTransStartingDate
  , COALESCE(NULLIF(PS.[Trans_ Ending Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS StatementTransEndingDate
FROM stage_nav.[Posted Statement] AS PS
LEFT JOIN stage_nav.[Store] AS STR
  ON STR.[No_] = PS.[Store No_]
    AND STR.Company_Id = PS.Company_Id
    AND STR.Data_Connection_Id = PS.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_M
  ON e_M.TableName = 'Posted Statement'
    AND e_M.ColumnName = 'Method'
    AND e_M.OptionNo = PS.[Method]
    AND e_M.DataConnectionId = PS.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_CM
  ON e_CM.TableName = 'Posted Statement'
    AND e_CM.ColumnName = 'Closing Method'
    AND e_CM.OptionNo = PS.[Closing Method]
    AND e_CM.DataConnectionId = PS.Data_Connection_Id
