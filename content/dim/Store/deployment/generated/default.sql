SELECT
  -- System Information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- Business Key
  , S.[No_] AS StoreCode
  -- Attributes                        
  , COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreDescription
  , COALESCE(NULLIF(S.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreCodeDescription
  , COALESCE(NULLIF(S.[Country Code], ''), 'N/A') AS StoreCountryCode
  , COALESCE(NULLIF(CR.[Name], ''), 'N/A') AS StoreCountryName
  , COALESCE(NULLIF(S.[Country Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(S.[Name], ''), 'N/A') AS StoreCountryCodeName
  , COALESCE(NULLIF(S.[Post Code], ''), 'N/A') AS StorePostCode
  , COALESCE(NULLIF(S.Address, ''), 'N/A') AS StoreAddress
  , COALESCE(NULLIF(S.City, ''), 'N/A') AS StoreCity
  , COALESCE(NULLIF(S.[Location Code], ''), 'N/A') AS StoreLocationCode
  , COALESCE(NULLIF(L.[Name], ''), 'N/A') AS StoreLocationDescription
  , COALESCE(NULLIF(S.[Location Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(L.[Name], ''), 'N/A') AS StoreLocationCodeDescription
  , COALESCE(NULLIF(S.[Currency Code], ''), 'N/A') AS CurrencyCode
FROM stage_nav.[Store] AS S
LEFT JOIN stage_nav.[Country_Region] AS CR
  ON CR.Code = S.[Country Code]
    AND CR.Company_Id = S.Company_Id
    AND CR.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN stage_nav.[Location] AS L
  ON L.Code = S.[Location Code]
    AND L.Company_Id = S.Company_Id
    AND L.Data_Connection_Id = S.Data_Connection_Id
