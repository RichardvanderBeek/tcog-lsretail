SELECT
  -- System Information
  ST.stage_id AS StageID
  , ST.company_id AS CompanyID
  , ST.data_connection_id AS DataConnectionID
  -- Business Key
  , ST.[Code] AS SalesTypeCode
  --  Attributes                        
  , COALESCE(NULLIF(ST.Description, ''), 'N/A') AS SalesTypeDescription
  , COALESCE(NULLIF(ST.[Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(ST.Description, ''), 'N/A') AS SalesTypeCodeDescription
  , COALESCE(NULLIF(e_ST.OptionValue, ''), 'N/A') AS SuspendTypeDescription
FROM stage_nav.[Sales Type] AS ST
LEFT JOIN help.Enumerations AS e_ST
  ON e_ST.TableName = 'Sales Type'
    AND e_ST.ColumnName = 'Suspend Type'
    AND e_ST.OptionNo = ST.[Suspend Type]
    AND e_ST.DataConnectionId = ST.Data_Connection_Id
