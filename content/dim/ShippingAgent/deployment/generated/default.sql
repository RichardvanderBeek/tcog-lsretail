SELECT
  -- system information
  SA.stage_id AS StageID
  , SA.company_id AS CompanyID
  , SA.data_connection_id AS DataConnectionID
  -- business key
  , SA.Code AS ShippingAgentCode
  --  attributes                        
  , COALESCE(NULLIF(SA.NAME, ''), 'N/A') AS ShippingAgentName
  , COALESCE(NULLIF(SA.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(SA.NAME, ''), 'N/A') AS ShippingAgentCodeName
FROM stage_nav.[Shipping Agent] AS SA
