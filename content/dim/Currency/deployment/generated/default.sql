SELECT
  -- System information
  CU.stage_id AS StageID
  , CU.company_id AS CompanyID
  , CU.data_connection_id AS DataConnectionID
  -- Business key
  , CU.Code AS CurrencyCode
  -- Attributes
  , COALESCE(NULLIF(CU.Description, ''), 'N/A') AS CurrencyDescription
  , COALESCE(NULLIF(CU.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(CU.Description, ''), 'N/A') AS CurrencyCodeDescription
FROM stage_nav.Currency AS CU
