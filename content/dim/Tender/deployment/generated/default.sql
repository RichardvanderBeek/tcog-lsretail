WITH Tender_CTE
AS (
  SELECT
    -- System Information
    A.stageid AS StageID
    , A.companyid AS CompanyID
    , A.dataconnectionid AS DataConnectionID
    -- Business Key
    , GL.[No_] AS Code
    -- Attributes                         
    , 0 AS AccountType
    , e_GL.OptionValue AS TypeDescription
    , GL.NAME AS TypeName
    , GL.[No_] + ' - ' + GL.NAME AS TypeCodeName
    , GL.NAME AS GroupName
    , GL.[No_] + ' - ' + GL.NAME AS GroupCodeName
  FROM (
    SELECT DISTINCT TT.stage_id AS StageID
      , TT.company_id AS CompanyID
      , TT.data_connection_id AS DataConnectionID
      , TT.[Account No_] AS Code
    FROM stage_nav.[Tender Type] AS TT
    WHERE TT.[Account Type] = 0 --gl
    
    UNION ALL
    
    SELECT DISTINCT TT2.stage_id AS StageID
      , TT2.company_id AS CompanyID
      , TT2.data_connection_id AS DataConnectionID
      , TT2.[Difference G_L Acc_] AS Code
    FROM stage_nav.[Tender Type] AS TT2
    
    UNION ALL
    
    SELECT DISTINCT TT3.stage_id AS StageID
      , TT3.company_id AS CompanyID
      , TT3.data_connection_id AS DataConnectionID
      , TT3.[Bank Account No_] AS Code
    FROM stage_nav.[Tender Type] AS TT3
    WHERE TT3.[Bank Account Type] = 0
    
    UNION ALL
    
    SELECT DISTINCT TTC.stage_id AS StageID
      , TTC.company_id AS CompanyID
      , TTC.data_connection_id AS DataConnectionID
      , TTC.[Account No_] AS Code
    FROM stage_nav.[Tender Type Card Setup] AS TTC
    WHERE TTC.[Account Type] = 0
    
    UNION ALL
    
    SELECT DISTINCT TTC2.stage_id AS StageID
      , TTC2.company_id AS CompanyID
      , TTC2.data_connection_id AS DataConnectionID
      , TTC2.[Difference G_L Acc_] AS Code
    FROM stage_nav.[Tender Type Card Setup] AS TTC2
    ) A
  INNER JOIN stage_nav.[G_L Account] AS GL
    ON A.Code = GL.[No_]
      AND A.CompanyId = GL.Company_Id
      AND A.DataConnectionId = GL.Data_Connection_Id
  CROSS JOIN help.Enumerations AS e_GL
  WHERE e_GL.OptionNo = TRY_CAST(A.Code AS INT)
    AND e_GL.ColumnName = 'Account Type'
    AND e_GL.TableName = 'Tender Type'
    AND e_GL.dataconnectionid = A.dataconnectionid
  
  UNION
  
  SELECT
    -- System Information
    A.stageid AS StageID
    , A.companyid AS CompanyID
    , A.dataconnectionid AS DataConnectionID
    -- Business Key
    , BAPG.[Code] AS Code
    -- Attributes
    , 1 AS AccountType
    , e_GL.OptionValue AS TypeDescription
    , BA.NAME AS TypeName
    , BA.[No_] + ' - ' + BA.NAME AS TypeCodeName
    , BAPG.[G_L Bank Account No_] AS GroupName
    , BAPG.Code + ' - ' + BAPG.[G_L Bank Account No_] AS GroupCodeName
  FROM (
    SELECT DISTINCT TT.stage_id AS StageID
      , TT.company_id AS CompanyID
      , TT.data_connection_id AS DataConnectionID
      , TT.[Account No_] AS Code
    FROM stage_nav.[Tender Type] AS TT
    WHERE TT.[Account Type] = 1 --BankAccount
    
    UNION ALL
    
    --SELECT DISTINCT TT2.stage_id AS StageID
    --  , TT2.company_id AS CompanyID
    --  , TT2.data_connection_id AS DataConnectionID
    --  , TT2.[Bank Account No_] AS Code
    --FROM stage_nav.[Tender Type] AS TT2
    --WHERE TT2.[Bank Account Type] = 2
    --UNION ALL
    SELECT DISTINCT TTC.stage_id AS StageID
      , TTC.company_id AS CompanyID
      , TTC.data_connection_id AS DataConnectionID
      , TTC.[Account No_] AS Code
    FROM stage_nav.[Tender Type Card Setup] AS TTC
    WHERE TTC.[Account Type] = 1
    ) A
  INNER JOIN stage_nav.[Bank Account] AS BA
    ON A.Code = BA.[No_]
      AND A.CompanyId = BA.Company_Id
      AND A.DataConnectionId = BA.Data_Connection_Id
  LEFT JOIN stage_nav.[Bank Account Posting Group] AS BAPG
    ON BA.[Bank Acc_ Posting Group] = BAPG.[Code]
      AND BA.company_id = BAPG.Company_Id
      AND BA.data_connection_id = BAPG.Data_Connection_Id
  CROSS JOIN help.Enumerations AS e_GL
  WHERE e_GL.OptionNo = TRY_CAST(A.Code AS INT)
    AND e_GL.ColumnName = 'Account Type'
    AND e_GL.TableName = 'Tender Type'
    AND e_GL.dataconnectionid = A.dataconnectionid
  )
SELECT
  -- System Information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- Business Key
  , S.[No_] AS StoreCode
  -- Attributes                        
  , COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreDescription
  , COALESCE(NULLIF(S.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreCodeDescription
  , COALESCE(NULLIF(S.[City], ''), 'N/A') AS StoreCityCode
  , COALESCE(NULLIF(S.[Location Code], ''), 'N/A') AS StoreLocationCode
  , COALESCE(NULLIF(TT.[Code], ''), 'N/A') AS TenderTypeCode
  , COALESCE(NULLIF(TT.[Description], ''), 'N/A') AS TenderTypeDescription
  , COALESCE(NULLIF(TT.[Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(TT.[Description], ''), 'N/A') AS TenderTypeCodeDescription
  , COALESCE(NULLIF(TT.[Account No_], ''), 'N/A') AS AccountCode
  , COALESCE(NULLIF(T.[TypeDescription], ''), 'N/A') AS AccountDescription
  , COALESCE(NULLIF(T.[Code], ''), 'N/A') AS GroupCode
  , COALESCE(NULLIF(T.[GroupName], ''), 'N/A') AS GroupDescription
  , COALESCE(NULLIF(TT.[Difference G_L Acc_], ''), 'N/A') AS DifferenceGLAccount
  , COALESCE(NULLIF(T2.[TypeDescription], ''), 'N/A') AS DifferenceGLAccountType
  , COALESCE(NULLIF(T2.[Code], ''), 'N/A') AS DifferenceGLAccountGroup
  , COALESCE(NULLIF(T2.[GroupName], ''), 'N/A') AS DifferenceGLAccountGroupDescription
  , COALESCE(NULLIF(TT.[Bank Account No_], ''), 'N/A') AS BankAccountNo
  , COALESCE(NULLIF(T3.[TypeDescription], ''), 'N/A') AS BankAccountDescription
  , COALESCE(NULLIF(T3.[Code], ''), 'N/A') AS BankAccountGroupCode
  , COALESCE(NULLIF(T3.[GroupName], ''), 'N/A') AS BankAccountGroupDescription
  , COALESCE(NULLIF(TT.[Bank Diff_ G_L Acc_], ''), 'N/A') AS BankDifferenceGLAccount
  , COALESCE(NULLIF(T4.[TypeDescription], ''), 'N/A') AS BankDifferenceGLAccountDescription
  , COALESCE(NULLIF(T4.[Code], ''), 'N/A') AS BankDifferenceGLAccountGroupCode
  , COALESCE(NULLIF(T4.[GroupName], ''), 'N/A') AS BankDifferenceGLAccountGroupDescription
  , COALESCE(NULLIF(TCS.[Card No_], ''), 'N/A') AS CardCode
  , COALESCE(NULLIF(TCS.[Description], ''), 'N/A') AS CardDescription
  , COALESCE(NULLIF(TCS.[Card No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(TCS.[Description], ''), 'N/A') AS CardCodeDescription
  , COALESCE(NULLIF(TCS.[Account No_], ''), 'N/A') AS CardAccountCode
  , COALESCE(NULLIF(T5.[TypeDescription], ''), 'N/A') AS CardAccountDescription
  , COALESCE(NULLIF(T5.[Code], ''), 'N/A') AS CardGroupCode
  , COALESCE(NULLIF(T5.[GroupName], ''), 'N/A') AS CardGroupDescription
  , COALESCE(NULLIF(TT.[Difference G_L Acc_], ''), 'N/A') AS BankDiffGLAccount
  , COALESCE(NULLIF(T4.[TypeDescription], ''), 'N/A') AS BankDiffGLAccountDescription
  , COALESCE(NULLIF(T4.[Code], ''), 'N/A') AS BankDiffGLAccountGroupCode
  , COALESCE(NULLIF(T4.[GroupName], ''), 'N/A') AS BankDiffGLAccountGroupDescription
  , COALESCE(NULLIF(e_MKC.[OptionValue], ''), 'N/A') AS ManagerKeyControl
  , COALESCE(NULLIF(e_KEA.[OptionValue], ''), 'N/A') AS KeyboardEntryAllowed
  , COALESCE(NULLIF(e_DO.[OptionValue], ''), 'N/A') AS DrawerOpens
  , COALESCE(NULLIF(e_TTB.[OptionValue], ''), 'N/A') AS TakenToBank
  , COALESCE(NULLIF(e_CR.[OptionValue], ''), 'N/A') AS CountingRequired
  , COALESCE(NULLIF(e_FC.[OptionValue], ''), 'N/A') AS ForeignCurrency
  , COALESCE(NULLIF(e_AT.[OptionValue], ''), 'N/A') AS AccountType
  , COALESCE(NULLIF(e_BAT.[OptionValue], ''), 'N/A') AS BankAccountType
  , COALESCE(NULLIF(e_TTS.[OptionValue], ''), 'N/A') AS TakenToSafe
FROM stage_nav.[Store] AS S
INNER JOIN stage_nav.[Tender Type] AS TT
  ON TT.[Store No_] = S.[No_]
    AND TT.Company_Id = S.Company_Id
    AND TT.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN stage_nav.[Tender Type Card Setup] AS TCS
  ON TCS.[Store No_] = S.[No_]
    AND TCS.Company_Id = S.Company_Id
    AND TCS.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN Tender_CTE AS T
  ON T.AccountType = TT.[Account Type]
    AND T.Code = TT.[Account No_]
    AND T.CompanyId = TT.Company_Id
    AND T.DataConnectionId = TT.Data_Connection_Id
LEFT JOIN Tender_CTE AS T2
  ON T2.AccountType = 0
    AND T2.Code = TCS.[Difference G_L Acc_]
    AND T2.CompanyId = TCS.Company_Id
    AND T2.DataConnectionId = TCS.Data_Connection_Id
LEFT JOIN Tender_CTE AS T3
  ON T3.AccountType = 1
    AND T3.Code = TT.[Bank Account No_]
    AND T3.CompanyId = TT.Company_Id
    AND T3.DataConnectionId = TT.Data_Connection_Id
LEFT JOIN Tender_CTE AS T4
  ON T4.AccountType = 0
    AND T4.Code = TT.[Bank Diff_ G_L Acc_]
    AND T4.CompanyId = TT.Company_Id
    AND T4.DataConnectionId = TT.Data_Connection_Id
LEFT JOIN Tender_CTE AS T5
  ON T5.AccountType = TCS.[Account Type]
    AND T5.Code = TCS.[Account No_]
    AND T5.CompanyId = TCS.Company_Id
    AND T5.DataConnectionId = TCS.Data_Connection_Id
LEFT JOIN Tender_CTE AS T6
  ON T6.AccountType = 0
    AND T6.Code = TCS.[Difference G_L Acc_]
    AND T6.CompanyId = TCS.Company_Id
    AND T6.DataConnectionId = TCS.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_MKC
  ON e_MKC.TableName = 'Account Type'
    AND e_MKC.ColumnName = 'YesNo'
    AND e_MKC.OptionNo = TT.[Manager Key Control]
    AND e_MKC.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_KEA
  ON e_KEA.TableName = 'Account Type'
    AND e_KEA.ColumnName = 'YesNo'
    AND e_KEA.OptionNo = TT.[Keyboard Entry Allowed]
    AND e_KEA.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_DO
  ON e_DO.TableName = 'Account Type'
    AND e_DO.ColumnName = 'YesNo'
    AND e_DO.OptionNo = TT.[Drawer Opens]
    AND e_DO.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_TTB
  ON e_TTB.TableName = 'Account Type'
    AND e_TTB.ColumnName = 'YesNo'
    AND e_TTB.OptionNo = TT.[Taken to Bank]
    AND e_TTB.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_CR
  ON e_CR.TableName = 'Account Type'
    AND e_CR.ColumnName = 'YesNo'
    AND e_CR.OptionNo = TT.[Counting Required]
    AND e_CR.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_FC
  ON e_FC.TableName = 'Account Type'
    AND e_FC.ColumnName = 'YesNo'
    AND e_FC.OptionNo = TT.[Foreign Currency]
    AND e_FC.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_AT
  ON e_KEA.TableName = 'Account Type'
    AND e_KEA.ColumnName = 'YesNo'
    AND e_KEA.OptionNo = TT.[Account Type]
    AND e_KEA.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_BAT
  ON e_BAT.TableName = 'Account Type'
    AND e_BAT.ColumnName = 'YesNo'
    AND e_BAT.OptionNo = TT.[Bank Account Type]
    AND e_BAT.DataConnectionId = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_TTS
  ON e_KEA.TableName = 'Account Type'
    AND e_KEA.ColumnName = 'YesNo'
    AND e_KEA.OptionNo = TT.[Taken to Safe]
    AND e_KEA.DataConnectionId = S.Data_Connection_Id
