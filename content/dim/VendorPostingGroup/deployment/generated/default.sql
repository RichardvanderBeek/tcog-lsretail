SELECT
  -- system information
  VPG.stage_id AS StageID
  , VPG.company_id AS CompanyID
  , VPG.data_connection_id AS DataConnectionID
  -- business key
  , VPG.Code AS VendorPostingGroupCode
  , VPG.Code AS VendorPostingGroupDesc
FROM stage_nav.[Vendor Posting Group] AS VPG
