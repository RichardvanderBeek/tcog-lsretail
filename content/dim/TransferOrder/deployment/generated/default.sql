SELECT
  -- System information
  SH.stage_id AS StageID
  , SH.company_id AS CompanyID
  , SH.data_connection_id AS DataConnectionID
  -- Business Key                    
  , SH.No_ AS DocumentCode
  -- Attribute
  , SH.[Status] AS SalesStatusCode
  , COALESCE(e_st.OptionValue, 'N/A') AS SalesStatusDescription
FROM stage_nav.[Transfer Header] AS SH
LEFT JOIN help.Enumerations AS e_st
  ON e_st.OptionNo = SH.[Status]
    AND e_st.ColumnName = 'Status'
    AND e_st.TableName = 'Transfer Header'
    AND e_st.dataconnectionid = SH.data_connection_id
