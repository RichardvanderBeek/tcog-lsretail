SELECT
  --System Information
  E_T.DataConnectionId AS DataConnectionID
  -- Business Key
  , E_T.OptionNo AS POSDiscTypeCode
  -- Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS POSDiscTypeDescription
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'POS Order Discount'
  AND E_T.ColumnName = 'DiscType'
