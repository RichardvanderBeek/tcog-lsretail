SELECT
  -- System Information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- Business Key
  , S.[No_] AS POSTerminalCode
  --  Attributes                 
  , COALESCE(ISNULL(S.[Description], ''), 'N/A') AS POSTerminalDescription
  , COALESCE(NULLIF(S.[No_], ''), 'N/A') + ' - ' + COALESCE(ISNULL(S.[Description], ''), 'N/A') AS POSTerminalCodeDescription
  , COALESCE(NULLIF(S.[Store No_], ''), 'N/A') AS StoreCode
  , COALESCE(NULLIF(S.[Store No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(STR.NAME, ''), 'N/A') AS StoreCodeName
  , COALESCE(NULLIF(S.[Placement], ''), 'N/A') AS Placement
  , COALESCE(NULLIF(S.[Hardware Profile], ''), 'N/A') AS HardwareProfile
  , COALESCE(NULLIF(S.[Menu Profile], ''), 'N/A') AS MenuProfile
  , COALESCE(NULLIF(S.[Interface Profile], ''), 'N/A') AS InterfaceProfile
  , COALESCE(NULLIF(S.[Functionality Profile], ''), 'N/A') AS FunctionalityProfile
FROM stage_nav.[POS Terminal] AS S
LEFT JOIN stage_nav.[Store] AS STR
  ON STR.[No_] = S.[Store No_]
    AND STR.Company_Id = S.Company_Id
    AND STR.Data_Connection_Id = S.Data_Connection_Id
