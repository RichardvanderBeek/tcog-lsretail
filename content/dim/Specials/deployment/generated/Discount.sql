SELECT
  -- System Information
  PD.stage_id AS StageID
  , PD.company_id AS CompanyID
  , PD.data_connection_id AS DataConnectionID
  -- Business Key   
  , PD.[No_] AS SpecialsCode
  -- Attributes
  , COALESCE(NULLIF(PD.[Description], ''), 'N/A') AS SpecialsDescription
  , COALESCE(NULLIF(PD.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PD.[Description], ''), 'N/A') AS SpecialsCodeDescription
  , COALESCE(NULLIF(e_S.[OptionValue], ''), 'N/A') AS SpecialStatus
  , COALESCE(NULLIF(e_T.[OptionValue], ''), 'N/A') AS SpecialType
  , COALESCE(NULLIF(VP.[Description], ''), 'N/A') AS ValidityDescription
FROM [stage_nav].[Periodic Discount] AS PD
LEFT JOIN stage_nav.[Validation Period] AS VP
  ON VP.[ID] = PD.[Validation Period ID]
    AND VP.Company_Id = PD.Company_Id
    AND VP.Data_Connection_Id = PD.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_S
  ON e_S.TableName = 'Periodic Discount'
    AND e_S.ColumnName = 'Status'
    AND e_S.OptionNo = PD.[Status]
    AND e_S.DataConnectionId = PD.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_T
  ON e_T.TableName = 'Periodic Discount'
    AND e_T.ColumnName = 'Type'
    AND e_T.OptionNo = PD.[Type]
    AND e_T.DataConnectionId = PD.Data_Connection_Id
