SELECT
  -- System Information
  O.stage_id AS StageID
  , O.company_id AS CompanyID
  , O.data_connection_id AS DataConnectionID
  -- Business Key
  , O.[No_] AS SpecialsCode
  -- Attributes
  , COALESCE(NULLIF(O.[Description], ''), 'N/A') AS SpecialsDescription
  , COALESCE(NULLIF(O.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(O.[Description], ''), 'N/A') AS SpecialsCodeDescription
  , COALESCE(NULLIF(e_S.[OptionValue], ''), 'N/A') AS SpecialStatus
  , COALESCE(NULLIF(e_T.[OptionValue], ''), 'N/A') AS SpecialType
  , COALESCE(NULLIF(VP.[Description], ''), 'N/A') AS ValidityDescription
FROM [stage_nav].[Offer] AS O
LEFT JOIN stage_nav.[Validation Period] AS VP
  ON VP.[ID] = O.[Validation Period ID]
    AND VP.Company_Id = O.Company_Id
    AND VP.Data_Connection_Id = O.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_S
  ON e_S.TableName = 'Offer'
    AND e_S.ColumnName = 'Status'
    AND e_S.OptionNo = O.[Status]
    AND e_S.DataConnectionId = O.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_T
  ON e_T.TableName = 'Offer'
    AND e_T.ColumnName = 'Type'
    AND e_T.OptionNo = O.[Type]
    AND e_T.DataConnectionId = O.Data_Connection_Id
