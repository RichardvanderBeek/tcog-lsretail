SELECT
  -- System Information
  VE.company_id AS CompanyID
  , VE.data_connection_id AS DataConnectionID
  -- Business Key                                                                                       
  , VE.[Document No_] AS DocumentNo
  , VE.[Document Type] AS DocumentTypeCode
  -- attributes 
  , COALESCE(NULLIF(e_dt.OptionValue, ''), 'N/A') AS DocumentTypeDescription
  , 1 AS DocumentInventoryKeyType
  , 'Value Entry' AS DocumentInventoryKeyTypeDesc
FROM stage_nav.[Value Entry] AS VE
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.OptionNo = VE.[Document Type]
    AND e_dt.TableName = 'Value Entry'
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.DataConnectionId = VE.data_connection_id
GROUP BY VE.company_id
  , VE.data_connection_id
  , VE.[Document No_]
  , VE.[Document Type]
  , COALESCE(NULLIF(e_dt.OptionValue, ''), 'N/A')
