SELECT
  -- System information
  PH.stage_id AS StageID
  , PH.company_id AS CompanyID
  , PH.data_connection_id AS DataConnectionID
  -- Business Key                    
  , PH.No_ AS DocumentCode
  , PH.[Document Type] AS DocumentTypeCode
  -- Attribute
  , COALESCE(e_dt.OptionValue, 'N/A') AS DocumentTypeDescription
  , PH.[Status] AS PurchaseStatusCode
  , COALESCE(e_st.OptionValue, 'N/A') AS PurchaseStatusDescription
FROM stage_nav.[Purchase Header] AS PH
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.TableName = 'Purchase Header'
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.OptionNo = PH.[Document Type]
    AND e_dt.DataConnectionId = PH.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_st
  ON e_st.OptionNo = PH.[Status]
    AND e_st.ColumnName = 'Status'
    AND e_st.TableName = 'Purchase Header'
    AND e_st.dataconnectionid = PH.data_connection_id
