SELECT
  -- system information
  IC.stage_id AS StageID
  , IC.company_id AS CompanyID
  , IC.data_connection_id AS DataConnectionID
  -- business key
  , IC.No_ AS ItemChargeCode
  --  attributes                        
  , COALESCE(NULLIF(IC.Description, ''), 'N/A') AS ItemChargeDescription
  , COALESCE(NULLIF(IC.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(IC.Description, ''), 'N/A') AS ItemChargeCodeDescription
FROM stage_nav.[Item Charge] AS IC
