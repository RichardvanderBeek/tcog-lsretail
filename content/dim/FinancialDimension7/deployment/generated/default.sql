SELECT DISTINCT fd.CompanyID
  , fd.DataConnectionID
  , fd.DimensionValueNo
  , fd.DimensionValueCode
  , fd.DimensionValueName
  , fd.DimensionValueCodeName
  , COALESCE(NULLIF(fdh.LevelCode0, ''), 'None') AS LevelCode0
  , COALESCE(NULLIF(fdh.LevelName0, ''), 'None') AS LevelName0
  , COALESCE(NULLIF(fdh.LevelCode0, ''), 'None') + ' - ' + COALESCE(NULLIF(fdh.LevelName0, ''), 'None') AS LevelCodeName0
  , COALESCE(NULLIF(fdh.LevelCode1, ''), 'None') AS LevelCode1
  , COALESCE(NULLIF(fdh.LevelName1, ''), 'None') AS LevelName1
  , COALESCE(NULLIF(fdh.LevelCode1, ''), 'None') + ' - ' + COALESCE(NULLIF(fdh.LevelName1, ''), 'None') AS LevelCodeName1
  , COALESCE(NULLIF(fdh.LevelCode2, ''), 'None') AS LevelCode2
  , COALESCE(NULLIF(fdh.LevelName2, ''), 'None') AS LevelName2
  , COALESCE(NULLIF(fdh.LevelCode2, ''), 'None') + ' - ' + COALESCE(NULLIF(fdh.LevelName2, ''), 'None') AS LevelCodeName2
  , COALESCE(NULLIF(fdh.LevelCode3, ''), 'None') AS LevelCode3
  , COALESCE(NULLIF(fdh.LevelName3, ''), 'None') AS LevelName3
  , COALESCE(NULLIF(fdh.LevelCode3, ''), 'None') + ' - ' + COALESCE(NULLIF(fdh.LevelName3, ''), 'None') AS LevelCodeName3
  , COALESCE(NULLIF(fdh.LevelCode4, ''), 'None') AS LevelCode4
  , COALESCE(NULLIF(fdh.LevelName4, ''), 'None') AS LevelName4
  , COALESCE(NULLIF(fdh.LevelCode4, ''), 'None') + ' - ' + COALESCE(NULLIF(fdh.LevelName4, ''), 'None') AS LevelCodeName4
FROM help.FinancialDimensions AS fd
LEFT JOIN help.FinancialDimensionHierarchies AS fdh
  ON fd.DimensionCode = fdh.DimensionCode
    AND fd.DimensionValueCode = fdh.Code
    AND fd.CompanyId = fdh.CompanyID
    AND fd.DataConnectionId = fdh.DataConnectionID
WHERE fd.DimensionGlobalSequence = 7
