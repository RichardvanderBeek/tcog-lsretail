SELECT
  -- system information
  B.stage_id AS StageID
  , B.company_id AS CompanyID
  , B.data_connection_id AS DataConnectionID
  -- business key                                     
  , B.Code AS BinCode
  --  attributes                     
  , COALESCE(NULLIF(B.[Location Code], ''), 'N/A') AS LocationCode
  , COALESCE(NULLIF(B.[Zone Code], ''), 'N/A') AS ZoneCode
  , COALESCE(NULLIF(B.Description, ''), 'N/A') AS BinDescription
  , COALESCE(NULLIF(B.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(B.Description, ''), 'N/A') AS BinCodeDescription
  , COALESCE(NULLIF(Z.Description, ''), 'N/A') AS ZoneDescription
  , COALESCE(NULLIF(B.[Zone Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(Z.Description, ''), 'N/A') AS ZoneCodeDescription
  , COALESCE(NULLIF(L.NAME, ''), 'N/A') AS LocationDescription
  , COALESCE(NULLIF(B.[Location Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(L.NAME, ''), 'N/A') AS LocationCodeDescription
FROM stage_nav.[Bin] AS B
LEFT JOIN stage_nav.Zone Z
  ON B.[Zone Code] = Z.Code
    AND B.[Location Code] = Z.[Location Code]
    AND B.company_id = Z.company_id
    AND B.data_connection_id = Z.data_connection_id
LEFT JOIN stage_nav.Location L
  ON B.[Location Code] = L.Code
    AND B.company_id = L.company_id
    AND B.data_connection_id = L.data_connection_id
