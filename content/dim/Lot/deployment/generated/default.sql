SELECT
  -- system information
  LNI.stage_id AS StageID
  , LNI.company_id AS CompanyID
  , LNI.data_connection_id AS DataConnectionID
  -- business key
  , LNI.[Item No_] AS ItemCode
  , LNI.[Variant Code] AS ItemVariantCode
  , LNI.[Lot No_] AS LotCode
  --attributes
  , COALESCE(NULLIF(LNI.[Description], ''), 'N/A') AS LotDescription
  , COALESCE(NULLIF(LNI.[Lot No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(LNI.[Description], ''), 'N/A') AS LotCodeDescription
  , COALESCE(LNI.[Test Quality], - 1) AS LotTestQuality
  , COALESCE(NULLIF(LNI.[Certificate Number], ''), 'N/A') AS LotCertificateNumber
  , COALESCE(be.OptionValue, 'N/A') AS LotBlocked
FROM stage_nav.[Lot No_ Information] LNI
LEFT JOIN help.Enumerations AS be
  ON be.TableName = 'Lot No_ Information'
    AND be.ColumnName = 'Blocked'
    AND be.OptionNo = LNI.Blocked
