SELECT
  -- System Information
  GPPG.stage_id AS StageID
  , GPPG.company_id AS CompanyID
  , GPPG.data_connection_id AS DataConnectionID
  -- Business Key
  , GPPG.Code AS GeneralProductPostingGroupCode
  -- Attributes
  , COALESCE(NULLIF(GPPG.Description, ''), 'N/A') AS GeneralProductPostingGroupDescription
  , COALESCE(NULLIF(GPPG.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(GPPG.Description, ''), 'N/A') AS GeneralProductPostingGroupCodeDescription
FROM stage_nav.[Gen_ Product Posting Group] AS GPPG
