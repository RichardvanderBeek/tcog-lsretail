SELECT
  -- System Information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- Business Key
  , S.[ID] AS StaffCode
  -- Attributes                        
  , COALESCE(ISNULL(S.[First Name], ''), 'N/A') + ' ' + COALESCE(ISNULL(S.[Last Name], ''), 'N/A') AS StaffName
  , COALESCE(ISNULL(S.[Last Name], ''), 'N/A') AS StaffLastName
  , COALESCE(NULLIF(S.[ID], ''), 'N/A') + ' - ' + COALESCE(ISNULL(S.[First Name], ''), 'N/A') + ' ' + COALESCE(ISNULL(S.[Last Name], ''), 'N/A') AS StaffCodeName
  , COALESCE(NULLIF(S.[Name on Receipt], ''), 'N/A') AS NameOnReceipt
  , COALESCE(NULLIF(S.[Store No_], ''), 'N/A') AS StoreCode
  , COALESCE(NULLIF(S.[Store No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(STR.NAME, ''), 'N/A') AS StoreCodeName
  , COALESCE(NULLIF(S.[Permission Group], ''), 'N/A') AS PermissionGroup
  , COALESCE(NULLIF(e_ST.[OptionValue], ''), 'N/A') AS EmploymentType
  , COALESCE(NULLIF(S.Address, ''), 'N/A') AS StaffAddress
  , COALESCE(NULLIF(S.[Post Code], ''), 'N/A') AS StaffPostCode
  , COALESCE(NULLIF(S.City, ''), 'N/A') AS StaffCity
  , COALESCE(NULLIF(S.[Sales Person], ''), 'N/A') AS StaffSalesPerson
FROM stage_nav.[Staff] AS S
LEFT JOIN stage_nav.[Store] AS STR
  ON STR.[No_] = S.[Store No_]
    AND STR.Company_Id = S.Company_Id
    AND STR.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_ST
  ON e_ST.TableName = 'Staff'
    AND e_ST.ColumnName = 'Employment Type'
    AND e_ST.OptionNo = S.[Employment Type]
    AND e_ST.DataConnectionId = S.Data_Connection_Id
