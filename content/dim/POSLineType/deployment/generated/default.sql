SELECT
  -- System Information
  A.DataConnectionId AS DataConnectionID
  -- Business Key
  , A.OptionNo AS POSItemSalesLevelCode
  -- Attributes
  , COALESCE(NULLIF(A.OptionValue, ''), 'N/A') AS POSItemSalesLevel
  , COALESCE(NULLIF(B.OptionValue, ''), 'N/A') AS POSStatementType
FROM help.Enumerations AS A
LEFT JOIN help.Enumerations AS B
  ON A.OptionNo = B.OptionNo
    AND B.TableName = 'Trans. Sales Entry status'
    AND B.ColumnName = 'Status'
    AND A.DataConnectionId = B.DataConnectionId
WHERE (
    A.TableName = 'Trans. Sales Entry'
    AND A.ColumnName = 'Type of Sale'
    )
