SELECT ag.AgeGroupID
  , ag.AgeGroupCode
  , ag.AgeGroupDesc
  , ag.AgeGroupMin
  , ag.AgeGroupMax
FROM (
  VALUES (
    0
    , '<0'
    , 'less than 0 days'
    , - 2147483648
    , - 1
    )
    , (
    1
    , '0-30'
    , 'between 0 and 30 days'
    , 0
    , 30
    )
    , (
    2
    , '31-60'
    , 'between 31 and 60 days'
    , 31
    , 60
    )
    , (
    3
    , '61-90'
    , 'between 61 and 90 days'
    , 61
    , 90
    )
    , (
    4
    , '>90'
    , 'more than 90 days'
    , 91
    , 2147483647
    )
  ) ag(AgeGroupID, AgeGroupCode, AgeGroupDesc, AgeGroupMin, AgeGroupMax)
