SELECT
  -- System Information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- Business Key
  , S.[No_] AS StoreCode
  -- Attribute
  , COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreDescription
  , COALESCE(NULLIF(S.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreCodeDescription
  , COALESCE(NULLIF(S.[Country Code], ''), 'N/A') AS StoreCountryCode
  , COALESCE(NULLIF(CR.[Name], ''), 'N/A') AS StoreCountryName
  , COALESCE(NULLIF(S.[Country Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(S.[Name], ''), 'N/A') AS StoreCountryCodeName
  , COALESCE(NULLIF(S.[Post Code], ''), 'N/A') AS StorePostCode
  , COALESCE(NULLIF(S.Address, ''), 'N/A') AS StoreAddress
  , COALESCE(NULLIF(S.City, ''), 'N/A') AS StoreCity
  , COALESCE(NULLIF(S.[Location Code], ''), 'N/A') AS StoreLocationCode
  , COALESCE(NULLIF(L.[Name], ''), 'N/A') AS StoreLocationDescription
  , COALESCE(NULLIF(S.[Location Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(L.[Name], ''), 'N/A') AS StoreLocationCodeDescription
  , COALESCE(NULLIF(IEA.[No_], ''), 'N/A') AS IncomeExpenseCode
  , COALESCE(NULLIF(IEA.[Description], ''), 'N/A') AS IncomeExpenseDescription
  , COALESCE(NULLIF(IEA.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IEA.[Description], ''), 'N/A') AS IncomeExpenseCodeDescription
  , COALESCE(NULLIF(e_AT.OptionValue, ''), 'N/A') AS AccountType
  , COALESCE(NULLIF(IEA.[G_L Account], ''), 'N/A') AS IncomeExpenseGLAccount
FROM stage_nav.[Store] AS S
LEFT JOIN stage_nav.[Country_Region] AS CR
  ON CR.Code = S.[Country Code]
    AND CR.Company_Id = S.Company_Id
    AND CR.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN stage_nav.[Location] AS L
  ON L.Code = S.[Location Code]
    AND L.Company_Id = S.Company_Id
    AND L.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN stage_nav.[Income_Expense Account] AS IEA
  ON IEA.[Store No_] = S.[No_]
    AND IEA.Company_Id = S.Company_Id
    AND IEA.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_AT
  ON e_AT.TableName = 'Income/Expense Account'
    AND e_AT.ColumnName = 'Account Type'
    AND e_AT.OptionNo = IEA.[Account Type]
    AND e_AT.DataConnectionId = S.Data_Connection_Id
