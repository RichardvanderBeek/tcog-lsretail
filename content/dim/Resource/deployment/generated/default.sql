SELECT
  -- system information
  RC.stage_id AS StageID
  , RC.company_id AS CompanyID
  , RC.data_connection_id AS DataConnectionID
  -- business key                                                                                             
  , RC.[No_] AS ResourceCode
  --  attributes                                                                                              
  , COALESCE(NULLIF(RC.[Name], ''), 'N/A') AS ResourceName
  , COALESCE(NULLIF(RC.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(RC.NAME, ''), 'N/A') AS ResourceCodeName
  , COALESCE(RC.[Type], 0) AS ResourceTypeCode
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS ResourceTypeDescription
  , COALESCE(NULLIF(RC.[Resource Group No_], ''), 'N/A') AS ResourceGroupCode
  , COALESCE(NULLIF(RG.[Name], ''), 'N/A') AS ResourceGroupDescription
  , COALESCE(NULLIF(RC.[Resource Group No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(RG.[Name], ''), 'N/A') AS ResourceGroupCodeDescription
FROM stage_nav.[Resource] AS RC
LEFT JOIN stage_nav.[Resource Group] AS RG
  ON RC.[Resource Group No_] = RG.[No_]
    AND RC.Company_ID = RG.Company_ID
    AND RC.Data_Connection_ID = RG.Data_Connection_ID
LEFT JOIN help.Enumerations AS E_T
  ON E_T.TableName = 'Resource'
    AND E_T.ColumnName = 'Type'
    AND RC.[Type] = E_T.OptionNo
    AND E_T.DataConnectionId = RC.Data_Connection_Id
