SELECT T.TimeID
  , T.[Time]
  , T.[Hours]
  , T.[Quarters]
  , T.[Minutes]
  , T.[Seconds]
  , T.TimeStrMinutes
  , T.TimeStrSecond
  , T.HourIntervalStr
  , T.QuarterIntervalStr
FROM help.[Time] AS T
