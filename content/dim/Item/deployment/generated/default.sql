SELECT
  -- system information
  IT.stage_id AS StageID
  , IT.company_id AS CompanyID
  , IT.data_connection_id AS DataConnectionID
  -- business key
  , IT.No_ AS ItemCode
  --  attributes                        
  , COALESCE(NULLIF(IT.Description, ''), 'N/A') AS ItemDescription
  , COALESCE(NULLIF(IT.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(IT.Description, ''), 'N/A') AS ItemCodeDescription
  , COALESCE(NULLIF(IT.[Base Unit of Measure], ''), 'N/A') AS ItemUnitOfMeasureCode
  , COALESCE(NULLIF(UOM.Description, ''), 'N/A') AS ItemUnitOfMeasureDescription
  , COALESCE(NULLIF(IT.[Base Unit of Measure], ''), 'N/A') + ' - ' + COALESCE(NULLIF(UOM.Description, ''), 'N/A') AS ItemUnitOfMeasureCodeDescription
  , COALESCE(NULLIF(IT.[Inventory Posting Group], ''), 'N/A') AS ItemInventoryPostingGroupCode
  , COALESCE(NULLIF(IPG.Description, ''), 'N/A') AS ItemInventoryPostingGroupDescription
  , COALESCE(NULLIF(IT.[Inventory Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IPG.Description, ''), 'N/A') AS ItemInventoryPostingGroupCodeDescription
  , COALESCE(NULLIF(IT.[Item Disc_ Group], ''), 'N/A') AS ItemDiscountGroupCode
  , COALESCE(NULLIF(IDG.Description, ''), 'N/A') AS ItemDiscountGroupDescription
  , COALESCE(NULLIF(IT.[Item Disc_ Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IDG.Description, ''), 'N/A') AS ItemDiscountGroupCodeDescription
  , COALESCE(NULLIF(IT.[Vendor No_], ''), 'N/A') AS ItemVendorCode
  , COALESCE(NULLIF(V.[Name], ''), 'N/A') AS ItemVendorName
  , COALESCE(NULLIF(IT.[Vendor No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(V.NAME, ''), 'N/A') AS ItemVendorCodeName
  , COALESCE(NULLIF(IT.[Gen_ Prod_ Posting Group], ''), 'N/A') AS ItemGenProdPostingGroupCode
  , COALESCE(NULLIF(GPPG.Description, ''), 'N/A') AS ItemGenProdPostingGroupDescription
  , COALESCE(NULLIF(IT.[Gen_ Prod_ Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(GPPG.Description, ''), 'N/A') AS ItemGenProdPostingGroupCodeDescription
  , COALESCE(NULLIF(IT.[Item Category Code], ''), 'N/A') AS ItemCategoryCode
  , COALESCE(NULLIF(IC.Description, ''), 'N/A') AS ItemCategoryDescription
  , COALESCE(NULLIF(IT.[Item Category Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IC.Description, ''), 'N/A') AS ItemCategoryCodeDescription
  , COALESCE(NULLIF(IT.[Product Group Code], ''), 'N/A') AS ItemProductGroupCode
  , COALESCE(NULLIF(PG.Description, ''), 'N/A') AS ItemProductGroupDescription
  , COALESCE(NULLIF(IT.[Product Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PG.Description, ''), 'N/A') AS ItemProductGroupCodeDescription
  , CAST(CAST(IT.[Unit Cost] AS DECIMAL(17,2)) AS NVARCHAR(255)) AS ItemUnitCost
FROM stage_nav.[Item] AS IT
LEFT JOIN stage_nav.[Unit Of Measure] AS UOM
  ON UOM.Code = IT.[Base Unit of Measure]
    AND UOM.company_id = IT.company_id
    AND UOM.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Inventory Posting Group] AS IPG
  ON IPG.Code = IT.[Inventory Posting Group]
    AND IPG.company_id = IT.company_id
    AND IPG.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Item Discount Group] AS IDG
  ON IDG.Code = IT.[Item Disc_ Group]
    AND IDG.company_id = IT.company_id
    AND IDG.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.Vendor AS V
  ON V.No_ = IT.[Vendor No_]
    AND V.company_id = IT.company_id
    AND V.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Gen_ Product Posting Group] AS GPPG
  ON GPPG.Code = IT.[Gen_ Prod_ Posting Group]
    AND GPPG.company_id = IT.company_id
    AND GPPG.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Item Category] AS IC
  ON IC.[Code] = IT.[Item Category Code]
    AND IC.company_id = IT.company_id
    AND IC.data_connection_id = IT.data_connection_id
LEFT JOIN stage_nav.[Product Group] AS PG
  ON PG.[Code] = IT.[Product Group Code]
    AND PG.[Item Category Code] = IT.[Item Category Code]
    AND PG.company_id = IT.company_id
    AND PG.data_connection_id = IT.data_connection_id
