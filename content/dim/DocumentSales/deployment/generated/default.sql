SELECT
  -- System information
  DS.StageID AS StageID
  , DS.CompanyID AS CompanyID
  , DS.DataConnectionID AS DataConnectionID
  -- Business Key                    
  , DS.DocumentNo AS DocumentNo
  , DS.DocumentType AS DocumentTypeCode
  -- Attribute
  , COALESCE(e_dt.OptionValue, 'N/A') AS DocumentTypeDescription
FROM (
  SELECT DISTINCT SSH.stage_id AS StageID
    , SSH.company_id AS CompanyID
    , SSH.data_connection_id AS DataConnectionID
    , SSH.[Posting Date] AS PostingDate
    , 1 AS DocumentType --Sales Shipment
    , SSH.[No_] AS DocumentNo
  FROM stage_nav.[Sales Shipment Header] AS SSH
  
  UNION ALL
  
  SELECT DISTINCT SIH.stage_id AS StageID
    , SIH.company_id AS CompanyID
    , SIH.data_connection_id AS DataConnectionID
    , SIH.[Posting Date] AS PostingDate
    , 2 AS DocumentType --Sales Invoice
    , SIH.[No_] AS DocumentNo
  FROM stage_nav.[Sales Invoice Header] AS SIH
  
  UNION ALL
  
  SELECT DISTINCT RRH.stage_id AS StageID
    , RRH.company_id AS CompanyID
    , RRH.data_connection_id AS DataConnectionID
    , RRH.[Posting Date] AS PostingDate
    , 3 AS DocumentType --Sales Return Receipt
    , RRH.[No_] AS DocumentNo
  FROM stage_nav.[Return Receipt Header] AS RRH
  
  UNION ALL
  
  SELECT DISTINCT SCMH.stage_id AS StageID
    , SCMH.company_id AS CompanyID
    , SCMH.data_connection_id AS DataConnectionID
    , SCMH.[Posting Date] AS PostingDate
    , 4 AS DocumentType --Sales Credit Memo
    , SCMH.[No_] AS DocumentNo
  FROM stage_nav.[Sales Cr_Memo Header] AS SCMH
  
  UNION ALL
  
  SELECT DISTINCT SERH.stage_id AS StageID
    , SERH.company_id AS CompanyID
    , SERH.data_connection_id AS DataConnectionID
    , SERH.[Posting Date] AS PostingDate
    , 11 AS DocumentType --Service Shipment Header
    , SERH.[No_] AS DocumentNo
  FROM stage_nav.[Service Shipment Header] AS SERH
  
  UNION ALL
  
  SELECT DISTINCT SERH.stage_id AS StageID
    , SERH.company_id AS CompanyID
    , SERH.data_connection_id AS DataConnectionID
    , SERH.[Posting Date] AS PostingDate
    , 12 AS DocumentType --Service Invoice Header
    , SERH.[No_] AS DocumentNo
  FROM stage_nav.[Service Invoice Header] AS SERH
  
  UNION ALL
  
  SELECT DISTINCT SCMH.stage_id AS StageID
    , SCMH.company_id AS CompanyID
    , SCMH.data_connection_id AS DataConnectionID
    , SCMH.[Posting Date] AS PostingDate
    , 13 AS DocumentType --Service Cr Memo Header
    , SCMH.[No_] AS DocumentNo
  FROM stage_nav.[Service Cr_Memo Header] AS SCMH
  ) DS
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.TableName = 'Item Ledger Entry'
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.OptionNo = DS.[DocumentType]
    AND e_dt.DataConnectionId = DS.DataConnectionID
