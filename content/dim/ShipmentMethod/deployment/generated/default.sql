SELECT
  -- system information
  SM.stage_id AS StageID
  , SM.company_id AS CompanyID
  , SM.data_connection_id AS DataConnectionID
  -- business key
  , SM.Code AS ShipmentMethodCode
  --  attributes                        
  , COALESCE(NULLIF(SM.Description, ''), 'N/A') AS ShipmentMethodDescription
  , COALESCE(NULLIF(SM.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(SM.Description, ''), 'N/A') AS ShipmentMethodCodeDescription
FROM stage_nav.[Shipment Method] AS SM
