SELECT
  -- System information
  C.stage_id AS StageID
  , C.company_id AS CompanyID
  , C.data_connection_id AS DataConnectionID
  -- Business key
  , C.No_ AS CustomerCode
  -- Attributes                        
  , COALESCE(NULLIF(C.NAME, ''), 'N/A') AS CustomerName
  , COALESCE(NULLIF(C.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(C.NAME, ''), 'N/A') AS CustomerCodeName
  , COALESCE(NULLIF(C.[Country_Region Code], ''), 'N/A') AS CustomerCountryCode
  , COALESCE(NULLIF(CO.NAME, ''), 'N/A') AS CustomerCountryDescription
  , COALESCE(NULLIF(C.[Country_Region Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CO.NAME, ''), 'N/A') AS CustomerCountryCodeDescription
  , COALESCE(NULLIF(C.[Territory Code], ''), 'N/A') AS CustomerTerritoryCode
  , COALESCE(NULLIF(T.NAME, ''), 'N/A') AS CustomerTerritoryDescription
  , COALESCE(NULLIF(C.[Territory Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(T.NAME, ''), 'N/A') AS CustomerTerritoryCodeDescription
  , COALESCE(NULLIF(C.City, ''), 'N/A') AS CustomerCity
  , COALESCE(NULLIF(C.[Post Code], ''), 'N/A') AS CustomerPostCode
  , COALESCE(NULLIF(C.[Customer Price Group], ''), 'N/A') AS CustomerPriceGroupCode
  , COALESCE(NULLIF(CPG.Description, ''), 'N/A') AS CustomerPriceGroupDescription
  , COALESCE(NULLIF(C.[Customer Price Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CDG.Description, ''), 'N/A') AS CustomerPriceGroupCodeDescription
  , COALESCE(NULLIF(C.[Customer Disc_ Group], ''), 'N/A') AS CustomerDiscountGroupCode
  , COALESCE(NULLIF(CDG.Description, ''), 'N/A') AS CustomerDiscountGroupDescription
  , COALESCE(NULLIF(C.[Customer Disc_ Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CDG.Description, ''), 'N/A') AS CustomerDiscountGroupCodeDescription
  , COALESCE(NULLIF(C.[Customer Posting Group], ''), 'N/A') AS CustomerPostingGroupCode
  , COALESCE(NULLIF(C.[Customer Posting Group], ''), 'N/A') AS CustomerPostingGroupDescription
  , COALESCE(NULLIF(C.[Customer Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(C.[Customer Posting Group], ''), 'N/A') AS CustomerPostingGroupCodeDescription
  , COALESCE(NULLIF(C.[Gen_ Bus_ Posting Group], ''), 'N/A') AS GeneralBusinessPostingGroupCode
  , COALESCE(NULLIF(GBPG.Description, ''), 'N/A') AS GeneralBusinessPostingGroupDescription
  , COALESCE(NULLIF(C.[Gen_ Bus_ Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(GBPG.Description, ''), 'N/A') AS GeneralBusinessPostingGroupCodeDescription
  , COALESCE(NULLIF(C.[Salesperson Code], ''), 'N/A') AS SalesPersonPurchaserCode
  , COALESCE(NULLIF(SP.NAME, ''), 'N/A') AS SalesPersonPurchaserName
  , COALESCE(NULLIF(C.[Salesperson Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(SP.NAME, ''), 'N/A') AS SalesPersonPurchaserCodeName
  , COALESCE(NULLIF(C.[Payment Terms Code], ''), 'N/A') AS PaymentTermsCode
  , COALESCE(NULLIF(PT.Description, ''), 'N/A') AS PaymentTermsDescription
  , COALESCE(NULLIF(C.[Payment Terms Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PT.Description, ''), 'N/A') AS PaymentTermsCodeDescription
FROM stage_nav.Customer AS C
LEFT JOIN stage_nav.Country_Region AS CO
  ON CO.Code = C.[Country_Region Code]
    AND CO.company_id = C.company_id
    AND CO.data_connection_id = C.data_connection_id
LEFT JOIN stage_nav.Territory AS T
  ON T.Code = C.[Territory Code]
    AND T.company_id = C.company_id
    AND T.data_connection_id = C.data_connection_id
LEFT JOIN stage_nav.Salesperson_Purchaser AS SP
  ON SP.Code = C.[Salesperson Code]
    AND SP.company_id = C.company_id
    AND SP.data_connection_id = C.data_connection_id
LEFT JOIN stage_nav.[Payment Terms] AS PT
  ON PT.Code = C.[Payment Terms Code]
    AND PT.company_id = C.company_id
    AND PT.data_connection_id = C.data_connection_id
LEFT JOIN stage_nav.[Customer Discount Group] AS CDG
  ON CDG.Code = C.[Customer Disc_ Group]
    AND CDG.company_id = C.company_id
    AND CDG.data_connection_id = C.data_connection_id
LEFT JOIN stage_nav.[Customer Price Group] AS CPG
  ON CPG.Code = C.[Customer Price Group]
    AND CPG.company_id = C.company_id
    AND CPG.data_connection_id = C.data_connection_id
LEFT JOIN stage_nav.[Gen_ Business Posting Group] AS GBPG
  ON GBPG.Code = C.[Gen_ Bus_ Posting Group]
    AND GBPG.company_id = C.company_id
    AND GBPG.data_connection_id = C.data_connection_id
