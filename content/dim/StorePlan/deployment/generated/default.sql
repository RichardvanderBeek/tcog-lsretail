WITH StoreSection_CTE
AS (
  SELECT
    -- System Information
    SS.stage_id AS StageID
    , SS.company_id AS CompanyID
    , SS.data_connection_id AS DataConnectionID
    -- Business Key
    , SS.[Store No_] AS StoreCode
    -- Attributes
    , SS.Code AS SectionCode
    , SS.Description AS SectionDescription
    , SS.[Section Size] AS SectionSize
    , e_S.OptionValue AS SizeIn
    , SSF.Code AS ShelfCode
    , SSF.Description AS ShelfDescription
    , SSF.[Section _] AS SectionPercent
    , CASE 
      WHEN (
          SS.[Section Size] <> 0
          AND SSF.[Section _] <> 0
          )
        THEN (SS.[Section Size] / 100 * SSF.[Section _])
      ELSE 0
      END AS ShelfSize
  FROM stage_nav.[store section] SS
  LEFT JOIN stage_nav.[Section Shelf] AS SSF
    ON SSF.[Store No_] = SS.[Store No_]
      AND SSF.Company_Id = SS.Company_Id
      AND SSF.Data_Connection_Id = SS.Data_Connection_Id
  LEFT JOIN help.Enumerations AS e_S
    ON e_S.OptionNo = SS.[Size In]
      AND e_S.ColumnName = 'Size In'
      AND e_S.TableName = 'Store Section'
      AND e_S.dataconnectionid = SS.data_connection_id
  )
SELECT
  -- System Information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- Business Key
  , S.[No_] AS StoreCode
  -- Attributes                        
  , COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreDescription
  , COALESCE(NULLIF(S.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(S.NAME, ''), 'N/A') AS StoreCodeDescription
  , COALESCE(NULLIF(S.[City], ''), 'N/A') AS StoreCityCode
  , COALESCE(NULLIF(S.[Location Code], ''), 'N/A') AS StoreLocationCode
  , COALESCE(NULLIF(CAST(S.[store size (square meters)] AS DECIMAL(19, 4)), '0'), '0') AS StoreSizeSquareMeters
  , COALESCE(NULLIF(SS.[SectionCode], ''), 'N/A') AS SectionCode
  , COALESCE(NULLIF(SS.[SectionDescription], ''), 'N/A') AS SectionDescription
  , COALESCE(NULLIF(SS.[SectionCode], ''), 'N/A') + ' - ' + COALESCE(NULLIF(SS.[SectionDescription], ''), 'N/A') AS SectionCodeDescription
  , COALESCE(NULLIF(CAST(SS.[SectionSize] AS DECIMAL(19, 4)), '0'), '0') AS SectionSize
  , COALESCE(NULLIF(SS.[SizeIn], ''), 'N/A') AS SectionSizeIn
  , COALESCE(NULLIF(SS.[ShelfCode], ''), 'N/A') AS ShelfCode
  , COALESCE(NULLIF(SS.[ShelfDescription], ''), 'N/A') AS ShelfDescription
  , COALESCE(NULLIF(SS.[ShelfCode], ''), 'N/A') + ' - ' + COALESCE(NULLIF(SS.[ShelfDescription], ''), 'N/A') AS ShelfCodeDescription
  , COALESCE(NULLIF(CAST(SS.[ShelfSize] AS DECIMAL(19, 4)), '0'), '0') AS ShelfSize
FROM stage_nav.[Store] AS S
LEFT JOIN stage_nav.[Country_Region] AS CR
  ON CR.Code = S.[Country Code]
    AND CR.Company_Id = S.Company_Id
    AND CR.Data_Connection_Id = S.Data_Connection_Id
LEFT JOIN stage_nav.[Location] AS L
  ON L.Code = S.[Location Code]
    AND L.Company_Id = S.Company_Id
    AND L.Data_Connection_Id = S.Data_Connection_Id
INNER JOIN StoreSection_CTE AS SS
  ON SS.StoreCode = S.[No_]
    AND SS.CompanyId = S.Company_Id
    AND SS.DataConnectionId = S.Data_Connection_Id
