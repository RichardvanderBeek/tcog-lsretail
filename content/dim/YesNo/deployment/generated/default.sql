SELECT 0 AS YesNoCode
  , 'No' AS YesNoDesc
  , 0 AS YesNoBit
  , 1 AS YesNoSortBy

UNION

SELECT 1 AS YesNoCode
  , 'Yes' AS YesNoDesc
  , 1 AS YesNoBit
  , 0 AS YesNoSortBy
