SELECT
  -- System Information
  IC.stage_id AS StageID
  , IC.company_id AS CompanyID
  , IC.data_connection_id AS DataConnectionID
  -- Business Key
  , IC.[Code] AS InfoCode
  -- Attributes
  , IC.[Code] + '-' + COALESCE(NULLIF(ISC.[Subcode], ''), 'N/A') AS InfoCodeSubCode
  , COALESCE(NULLIF(IC.[Description], ''), 'N/A') AS InfoDescription
  , COALESCE(NULLIF(IC.[Code], ''), 'N/A') + ' ' + COALESCE(NULLIF(IC.[Description], ''), 'N/A') AS InfoCodeDescription
  , COALESCE(NULLIF(ISC.[Subcode], ''), 'N/A') AS SubCode
  , COALESCE(NULLIF(ISC.[Description], ''), 'N/A') AS SubDescription
  , COALESCE(NULLIF(ISC.[SubCode], ''), 'N/A') + ' - ' + COALESCE(NULLIF(ISC.[Description], ''), 'N/A') AS SubCodeDescription
  , COALESCE(NULLIF(e_IC.[OptionValue], ''), 'N/A') AS InfoCodeType
  , COALESCE(NULLIF(IC.[Data Entry Type], ''), 'N/A') AS POSDataEntryType
FROM [stage_nav].[Infocode] AS IC
LEFT JOIN stage_nav.[Information Subcode] AS ISC
  ON ISC.[Code] = IC.[Code]
    AND ISC.Company_Id = IC.Company_Id
    AND ISC.Data_Connection_Id = IC.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_IC
  ON e_IC.TableName = 'InfoCode'
    AND e_IC.ColumnName = 'Type'
    AND e_IC.OptionNo = IC.[Type]
    AND e_IC.DataConnectionId = IC.Data_Connection_Id
