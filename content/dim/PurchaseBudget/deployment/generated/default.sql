SELECT
  -- system information
  IBN.stage_id AS StageID
  , IBN.company_id AS CompanyID
  , IBN.data_connection_id AS DataConnectionID
  -- business key
  , IBN.NAME AS PurchaseBudgetCode
  --  attributes                        
  , COALESCE(NULLIF(IBN.Description, ''), 'N/A') AS PurchaseBudgetDescription
  , COALESCE(NULLIF(IBN.NAME, ''), 'N/A') + ' - ' + COALESCE(NULLIF(IBN.Description, ''), 'N/A') AS PurchaseBudgetCodeDescription
FROM stage_nav.[Item Budget Name] AS IBN
WHERE IBN.[Analysis Area] = 1;-- Purchase
