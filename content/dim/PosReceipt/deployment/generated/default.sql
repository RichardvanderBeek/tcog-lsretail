SELECT
  -- System Information
  TH.stage_id AS StageID
  , TH.company_id AS CompanyID
  , TH.data_connection_id AS DataConnectionID
  -- Business Key
  , TH.[Transaction No_] AS TransactionCode
  , COALESCE(NULLIF(TH.[Store No_], ''), 'N/A') AS StoreCode
  , COALESCE(NULLIF(TH.[POS Terminal No_], ''), 'N/A') AS POSTerminalCode
  , COALESCE(NULLIF(TH.[Receipt No_], ''), 'N/A') AS ReceiptCode
  , COALESCE(NULLIF(TPE.[Tender Type], ''), 'N/A') AS TenderType
  -- Attributes
  , COALESCE(NULLIF(STR.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(STR.NAME, ''), 'N/A') AS StoreCodeDescription
  , COALESCE(NULLIF(STR.City, ''), 'N/A') AS StoreCity
  , COALESCE(NULLIF(e_TT.OptionValue, ''), 'N/A') AS TransactionType
  , COALESCE(NULLIF(e_TC.OptionValue, ''), 'N/A') AS TransactionGroup
  , COALESCE(NULLIF(TH.[Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS ReceiptDate
  , COALESCE(NULLIF(TPE.[Card No_], ''), 'N/A') AS TenderTypeCardCode
  , COALESCE(NULLIF(CAST(TPE.[Line No_] AS NVARCHAR(40)), ''), 'N/A') AS TenderTypeLineNo
  , COALESCE(NULLIF(TIE.[Infocode], ''), 'N/A') AS InfoCode
  , COALESCE(NULLIF(TIE.[Subcode], ''), 'N/A') AS InfoCodeSubCode
  , COALESCE(NULLIF(CAST(TIE.[Line No_] AS NVARCHAR(40)), ''), 'N/A') AS InfoCodeLineNo
  , COALESCE(NULLIF(CONVERT(NVARCHAR(255), CONVERT(DATETIME, DATEADD(mi, DATEDIFF(mi, 0, TH.[Time]), 0)), 108), ''), 'N/A') AS ReceiptTime
FROM stage_nav.[Transaction Header] AS TH
LEFT JOIN stage_nav.[Store] AS STR
  ON STR.[No_] = TH.[Store No_]
    AND STR.Company_Id = TH.Company_Id
    AND STR.Data_Connection_Id = TH.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_TT
  ON e_TT.TableName = 'Transaction Header'
    AND e_TT.ColumnName = 'Transaction Type'
    AND e_TT.OptionNo = TH.[Transaction Type]
    AND e_TT.DataConnectionId = TH.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_TC
  ON e_TT.TableName = 'Transaction Header'
    AND e_TT.ColumnName = 'Transaction Code'
    AND e_TT.OptionNo = TH.[Transaction Type]
    AND e_TT.DataConnectionId = TH.Data_Connection_Id
LEFT JOIN stage_nav.[Trans_ Payment Entry] AS TPE
  ON TPE.[Transaction No_] = TH.[Transaction No_]
    AND TPE.[Store No_] = TH.[Store No_]
    AND TPE.[POS Terminal No_] = TH.[POS Terminal No_]
    AND TPE.[Change line] = 0
    AND TPE.Company_Id = TH.Company_Id
    AND TPE.Data_Connection_Id = TH.Data_Connection_Id
LEFT JOIN stage_nav.[Trans_ Infocode Entry] AS TIE
  ON TIE.[Transaction No_] = TH.[Transaction No_]
    AND TIE.[Store No_] = TH.[Store No_]
    AND TIE.[POS Terminal No_] = TH.[POS Terminal No_]
    AND TIE.[Infocode] <> 'TEXT'
    AND TIE.Company_Id = TH.Company_Id
    AND TIE.Data_Connection_Id = TH.Data_Connection_Id
