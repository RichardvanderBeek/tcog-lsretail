SELECT
  -- System Information
  E.stage_id AS StageID
  , E.company_id AS CompanyID
  , E.data_connection_id AS DataConnectionID
  -- Business Key
  , E.No_ AS EmployeeCode
  -- Attributes
  , COALESCE(NULLIF(COALESCE(NULLIF(E.[First Name], '') + ' ', '') + COALESCE(NULLIF(E.[Middle Name], '') + ' ', '') + COALESCE(NULLIF(E.[Last Name], ''), ''), ''), 'N/A') AS EmployeeName
  , COALESCE(NULLIF(COALESCE(NULLIF(E.[Middle Name], '') + ' ', '') + COALESCE(NULLIF(E.[Last Name], ''), ''), ''), 'N/A') AS EmployeeLastName
  , COALESCE(ISNULL(E.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(COALESCE(NULLIF(E.[First Name], '') + ' ', '') + COALESCE(NULLIF(E.[Middle Name], '') + ' ', '') + COALESCE(NULLIF(E.[Last Name], ''), ''), ''), 'N/A') AS EmployeeCodeName
FROM stage_nav.Employee AS E
